import { fromPairs, map, pair, reject, toPairs, toPairsIn } from 'ramda'

var sss = {
  products: {
    c12d529482f98bd89d953c6960e9684a: {
      id: "438",
      price: 1440,
      weight: 12,
      count: "2",
      options: {
        composition: "192,345,345,345,372,372,85,85,86,86,86,86",
        box: "438",
        volume: 1,
        length: 1
      },
      boxes: [
        {
          id: 436,
          name: "Счастье для 6 макарони",
          size: 15,
          description: "Сроки хранения: до 15 суток при температуре не выше 15°C",
          introtext: "Условия доставки: Экспресс доставка в течение трех часов в пределах КАД",
          content: "<p>1</p>",
          price: 360,
          article: "001002",
          image_first: "/assets/images/products/436/upa.jpg",
          storing: "до 15 суток при температуре не выше 15°C",
          technical: "6",
          delivering: "Экспресс доставка в течение трех часов в пределах КАД",
          active: "false",
          images: ["/assets/images/products/436/upa.jpg", "/assets/images/products/436/mac6new2.jpg"],
          tags: []
        },
        {
          id: 438,
          name: "счастье для 12 макарони",
          size: 12,
          description:
            "Наши макарони созданы исключительно из натуральных ингредиентов, мы соблюдаем классические французские рецепты, дополняя их авторскими идеями.",
          introtext: "Условия доставки: Экспресс доставка в течение трех часов в пределах КАД",
          content: "<p>1</p>",
          price: 720,
          article: "001002",
          image_first: "/assets/images/products/438/0.jpg",
          storing: "до 15 суток при температуре не выше 15°C",
          technical: "6",
          delivering: "Экспресс доставка в течение трех часов в пределах КАД",
          active: "true",
          images: ["/assets/images/products/438/0.jpg", "/assets/images/products/438/1.jpg"],
          tags: []
        }
      ],
      ctx: "web"
    },
    c12d529482f98bd89d953c6960e9684a2: {
      id: "438",
      price: 1440,
      weight: 12,
      count: "2",
      options: {
        composition: "192,345,345,345,372,372,85,85,86,86,86,86",
        box: "438",
        volume: 1,
        length: 1
      },
      boxes: [
        {
          id: 436,
          name: "Счастье для 6 макарони",
          size: 15,
          description: "Сроки хранения: до 15 суток при температуре не выше 15°C",
          introtext: "Условия доставки: Экспресс доставка в течение трех часов в пределах КАД",
          content: "<p>1</p>",
          price: 360,
          article: "001002",
          image_first: "/assets/images/products/436/upa.jpg",
          storing: "до 15 суток при температуре не выше 15°C",
          technical: "6",
          delivering: "Экспресс доставка в течение трех часов в пределах КАД",
          active: "false",
          images: ["/assets/images/products/436/upa.jpg", "/assets/images/products/436/mac6new2.jpg"],
          tags: []
        },
        {
          id: 438,
          name: "счастье для 12 макарони",
          size: 12,
          description:
            "Наши макарони созданы исключительно из натуральных ингредиентов, мы соблюдаем классические французские рецепты, дополняя их авторскими идеями.",
          introtext: "Условия доставки: Экспресс доставка в течение трех часов в пределах КАД",
          content: "<p>1</p>",
          price: 720,
          article: "001002",
          image_first: "/assets/images/products/438/0.jpg",
          storing: "до 15 суток при температуре не выше 15°C",
          technical: "6",
          delivering: "Экспресс доставка в течение трех часов в пределах КАД",
          active: "true",
          images: ["/assets/images/products/438/0.jpg", "/assets/images/products/438/1.jpg"],
          tags: []
        }
      ],
      ctx: "web"
    }
  }
}


const item = toPairsIn(sss.products)
const total = item.reduce((acc, item) => acc + (item[1].price * item[1].count), 0)
console.log(total)