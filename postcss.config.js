module.exports = {
  plugins: [
    // require('postcss-assets')({
    //   loadPaths: ['source/icons/', 'source/images/', 'source/svg']
    // }),
    require('lost')(),
    require('postcss-color-function')(),
    // require('postcss-import')(),
    require('postcss-short')(),
    // require('postcss-clearfix')(),
    // require('postcss-short'),
    require('postcss-easings'),
    // require('postcss-merge-longhand'),
    require('postcss-center'),
    require('postcss-flexbox'),
    require('postcss-flexbugs-fixes'),
    // require('postcss-flexibility'),
    // require('autoprefixer'),
    // require('postcss-triangle'),
    // require('postcss-responsive-type'),
    require('postcss-nested'),
    require('postcss-css-reset')
  ]
}
