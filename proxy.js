let http = require('http'),
  httpProxy = require('http-proxy')
//
// Create your proxy server and set the target in the options.
//
let proxy = httpProxy.createProxyServer({ target: 'http://lavka.monkeylab.ru/' }) // See (†)

//
// Create your target server
//
http
  .createServer(function(req, res) {
    proxy.web(req, res)
  })
  .listen(5050)
