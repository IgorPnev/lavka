import { call, put, takeEvery } from 'redux-saga/effects'
import { fromPairs, map, reject, toPairsIn } from 'ramda'

import { createSelector } from 'reselect'

const CART_HAS_ERRORED = 'CART_HAS_ERRORED'
const CART_FETCH_DATA_SUCCESS = 'CART_FETCH_DATA_SUCCESS'
const CART_IS_LOADING = 'CART_IS_LOADING'
const CART_IS_LOAD = 'CART_IS_LOAD'
const CART_REQUEST = 'CART_REQUEST'
const PRODUCT_INCRAMENT = 'PRODUCT_INCRAMENT'
const PRODUCT_DELETE = 'PRODUCT_DELETE'

const initialState = {
  isLoad: false,
  isLoading: false,
  hasErrored: false,
  items: []
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CART_IS_LOAD:
      return { ...state, isLoad: action.isLoad }
    case CART_IS_LOADING:
      return { ...state, isLoading: action.isLoading }
    case CART_HAS_ERRORED:
      return { ...state, hasErrored: action.hasErrored }
    case CART_FETCH_DATA_SUCCESS:
      return { ...state, items: action.items }
    case PRODUCT_INCRAMENT:
      return { ...state, items: incrament(state, action) }
    case PRODUCT_DELETE:
      ///deleteItem(state, action)
      return { ...state, items: deleteItem(state, action) }
    default:
      return state
  }
}
export function cartHasErrored(bool) {
  return {
    type: CART_HAS_ERRORED,
    hasErrored: bool
  }
}

export function cartIsLoading(bool) {
  return {
    type: CART_IS_LOADING,
    isLoading: bool
  }
}

export function cartIsLoad(bool) {
  return {
    type: CART_IS_LOAD,
    isLoad: bool
  }
}

export function cartFetchDataSuccess(cart) {
  return {
    type: CART_FETCH_DATA_SUCCESS,
    items: cart
  }
}

export function productIncrament(item) {
  return {
    type: PRODUCT_INCRAMENT,
    item: item
  }
}

export function productDelete(item) {
  return {
    type: PRODUCT_DELETE,
    item: item
  }
}

export function requestCart() {
  return { type: CART_REQUEST }
}

export function* watchFetchCart() {
  yield takeEvery(CART_REQUEST, fetchCartAsync)
}

function* fetchCartAsync() {
  try {
    yield put(cartIsLoading(true))
    const data = yield call(() => {
      // return fetch('http://localhost:5000/get-cart').then(res => res.json())
      return fetch('/api/get-cart', {
        method: 'get',
        credentials: 'include',
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
    })
    yield put(cartFetchDataSuccess(data))
    yield put(cartIsLoading(false))
    yield put(cartIsLoad(true))
  } catch (error) {
    yield put(cartHasErrored())
  }
}

export function cartFetchData(url) {
  return dispatch => {
    dispatch(cartIsLoading(true))

    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response
      })
      .then(response => response.json())
      .then(cart => dispatch(cartFetchDataSuccess(cart)))
      .then(() => dispatch(cartIsLoading(false)))
      .then(() => dispatch(cartIsLoad(true)))
      .catch(() => dispatch(cartHasErrored(true)))
  }
}

export function* cartSaga() {
  yield [watchFetchCart()]
}

const incrament = (state, action) => {
  const arr = map(item => {
    if (item[0] === action.item.id) {
      if (action.item.type === 'plus') item[1] = { ...item[1], count: item[1].count + 1 }
      if (action.item.type === 'minus') {
        if (item[1].count != 1) {
          item[1] = { ...item[1], count: item[1].count - 1 }
        }
      }
      return item
    }
    return item
  }, toPairsIn(state.items.products))

  return { products: fromPairs(arr) }
}

const deleteItem = (state, action) => {
  return { products: fromPairs(reject(item => item[0] === action.item.id)(toPairsIn(state.items.products))) }
}

// Selector
const getState = state => state.cart.items.products
const cartPercentSelector = state => state.discount.discount.value

export const subtotalSelector = createSelector(getState, items =>
  toPairsIn(items).reduce((acc, item) => acc + item[1].price * item[1].count, 0)
)

export const taxSelector = createSelector(
  subtotalSelector,
  cartPercentSelector,
  (subtotal, cartPercent) => subtotal * (cartPercent / 100)
)

export const totalSelector = createSelector(
  subtotalSelector,
  taxSelector,
  (subtotal, cartPercent) => subtotal - cartPercent
)
