import './cart.styl'

import React, { Component } from 'react'

import { BounceLoader } from 'react-spinners'
import Item from './item'
import { Scrollbars } from 'react-custom-scrollbars'
import { toPairs } from 'ramda'

class Carts extends Component {
  state = {}

  render() {
    const items = toPairs(this.props.carts.items.products)
    return (
      <div className="cart">
        <Scrollbars style={{ width: '100%' }}>
          <BounceLoader color={'#e9f0e0'} loading={this.props.carts.isLoading} />
          {this.props.carts.isLoad &&
            items.map((item, index) => {
              return (
                <Item
                  item={item}
                  key={'cartitem' + index}
                  productIncrament={this.props.productIncrament}
                  productDelete={this.props.productDelete}
                />
              )
            })}
        </Scrollbars>
      </div>
    )
  }
}
export default Carts
