import React, { Component } from 'react'

import ReactSuperSelect from 'react-super-select'
import css from 'styled-jsx/css'

let groceryCartHandler = function(item) {
  console.log('Add To Cart: ', item.name, ' ', 'Price: ', item.price)
}

const groceryCartItemTemplate = item => {
  return (
    <div className="boxItem" key={'kd' + item}>
      <img className="boxItemImage" src={'http://www.lavka.monkeylab.ru/' + item.image_first} alt="" />
      <p className="boxItemText">{item.name}</p>
    </div>
  )
}
class BoxSelect extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return [
      <ReactSuperSelect
        key="box"
        placeholder="Pick an Item"
        searchPlaceholder="Search shop"
        searchable={false}
        clearable={false}
        onChange={groceryCartHandler}
        customOptionTemplateFunction={groceryCartItemTemplate}
        dataSource={this.props.box}
        initialValue={this.props.box[0]}
      />,
      <style jsx="true" key="style">
        {styles}
      </style>
    ]
  }
}

export default BoxSelect

const styles = css`
  .boxItem {
    box: horizontal middle;
  }
  .boxItemImage {
    size: 24px;
    margin: 0 5px 0 0;
    border-radius: 50%;
    img {
      size: 100%;
      border-radius: 50%;
    }
  }
  .boxItemText {
    color: #4a4a4a;
    font-size: 11px;
    font-weight: 400;
    letter-spacing: 0.92px;
    white-space: nowrap;
  }

  .r-ss-wrap {
    position: relative;
    width: 100%;
    background-clip: padding-box;
    box-sizing: border-box;
  }
  .r-ss-wrap.r-ss-expanded {
    z-index: 1;
    background-color: #e9f0e0;
  }
  .r-ss-wrap * {
    box-sizing: border-box;
  }
  .r-ss-button {
    background: none;
    background-color: none;
    border: none;
  }
  .r-ss-trigger {
    position: relative;
    display: block;
    line-height: 30px;
    padding: 11px 30px 11px 9px;
    background-color: #f5eedd;
    text-decoration: none;
    color: #333;
    background-clip: padding-box;
    cursor: pointer;
    outline: none;
    word-wrap: break-word;
  }

  .r-ss-trigger.r-ss-disabled {
    opacity: 0.6;
    color: rgba(51, 51, 51, 0.45);
  }
  .r-ss-trigger.r-ss-disabled:focus {
    border-color: rgba(51, 51, 51, 0.45);
  }
  .r-ss-trigger.r-ss-open {
    background-clip: padding-box;
  }
  .r-ss-trigger.r-ss-open:focus {
    border-color: rgba(51, 51, 51, 0.45);
  }
  .r-ss-trigger.r-ss-placeholder {
    color: rgba(51, 51, 51, 0.45);
  }
  .r-ss-trigger:focus {
    border-color: black;
  }
  .r-ss-trigger .carat {
    position: absolute;
    right: 17px;
    size: 10px;
    background-repeat: no-repeat;
    bottom: 18px;
  }
  .r-ss-trigger .carat.up {
    background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg width='10' height='7' viewBox='0 0 10 7' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M10 1L4.99 6 0 1.02' stroke='%234A4A4A' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'/%3e%3c/svg%3e");
  }
  .r-ss-trigger .carat.down {
    background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg width='10' height='7' viewBox='0 0 10 7' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M10 1L4.99 6 0 1.02' stroke='%234A4A4A' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'/%3e%3c/svg%3e");
  }
  .r-ss-dropdown {
    position: absolute;
    top: 99%;
    background-clip: padding-box;
    background-color: #e9f0e0;
    max-height: 350px;
    width: 100%;
  }
  .r-ss-dropdown .r-ss-options-wrap {
    position: relative;
    max-height: 316px;
    width: 100%;
    overflow-y: auto;
  }
  .r-ss-dropdown .r-ss-dropdown-options {
    list-style: none;
    padding: 0;
    margin: 0;
  }

  .r-ss-dropdown .r-ss-dropdown-option {
    position: relative;
    left: 0;
    top: 0;
    width: 100%;
    word-wrap: break-word;
    padding: 11px 9px;
    line-height: 30px;
    border-bottom: 1px solid rgba(51, 51, 51, 0.1);
    cursor: pointer;
    user-select: none;
    -ms-user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
  }
  .r-ss-dropdown .r-ss-dropdown-option.r-ss-disabled {
    background-color: rgba(51, 51, 51, 0.1);
    opacity: 0.5;
  }
  .r-ss-dropdown .r-ss-dropdown-option:last-child {
    border-bottom: none;
  }
  .r-ss-dropdown .r-ss-dropdown-option:hover {
    background-color: rgba(51, 51, 51, 0.1);
  }
  .r-ss-dropdown .r-ss-dropdown-option.active {
    background-color: rgba(51, 51, 51, 0.2);
  }
  .r-ss-dropdown .r-ss-dropdown-option.r-ss-selected {
    background-color: #ffd;
  }
  .r-ss-dropdown .r-ss-dropdown-option.r-ss-selected:focus,
  .r-ss-dropdown .r-ss-dropdown-option.r-ss-selected::selection {
    background-color: #ffd;
  }
  .r-ss-dropdown .r-ss-dropdown-option:focus,
  .r-ss-dropdown .r-ss-dropdown-option::selection {
    outline: none;
    background-color: rgba(0, 136, 204, 0.1);
  }
`
