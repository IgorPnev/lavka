// import { eqProps, filter, find, propEq } from 'ramda'
import './item.styl'

import React, { Component } from 'react'

import BoxSelect from './item_boxes'

class Item extends Component {
  state = {}

  render() {
    const item = this.props.item[1]
    // const box = find(propEq('id', this.props.item[1].options.box))(this.props.item[1].boxes)
    const box = this.props.item[1].boxes[0]
    return (
      <div className="item">
        <div className="top">
          <div className="delete" onClick={() => this.props.productDelete({ id: this.props.item[0] })}>
            <svg height="8" width="9">
              <path d="M.94 7.58L7.994.527M7.918 7.505L1.015.602" fill="none" stroke="#9B9B9B" strokeLinecap="square" />
            </svg>
          </div>
          <div className="image">
            <img src={'http://www.lavka.monkeylab.ru/' + box.image_first} alt="" />
          </div>
          <div className="content">
            <div className="name">{box.name}</div>
            <div className="price">{item.price}</div>
          </div>
          <div className="qty">
            <button
              className="qtyPlus"
              onClick={() => this.props.productIncrament({ id: this.props.item[0], type: 'plus' })}>
              <svg height="8" width="16" viewBox="0 0 16 8">
                <path d="M1.099 7.899L7.847.954l6.72 6.916" fill="none" stroke="#4A4A4A" strokeLinecap="round" />
              </svg>
            </button>
            <div className="qtyNumber">{item.count}</div>
            <button
              className="qtyMinus"
              onClick={() => this.props.productIncrament({ id: this.props.item[0], type: 'minus' })}>
              <svg height="8" width="16" viewBox="0 0 16 8">
                <path d="M14.901.101L8.153 7.046 1.433.13" fill="none" stroke="#4A4A4A" strokeLinecap="round" />
              </svg>
            </button>
          </div>
        </div>
        <div className="bottom">
          <BoxSelect box={item.boxes} />
        </div>
      </div>
    )
  }
}

export default Item
