import './CartContainer.styl'

import {
  cartFetchData,
  productDelete,
  productIncrament,
  requestCart,
  subtotalSelector,
  taxSelector,
  totalSelector
} from './CartReducer'

import Carts from './components/cart/cart'
import React from 'react'
import { connect } from 'react-redux'

class CartContainer extends React.Component {
  componentDidMount() {
    this.props.requestCart()
  }

  render() {
    return (
      <div className="cartWrapper">
        <div className="total">
          {this.props.percent > 0 ? (
            <div className="totalPrice">
              Итого со скидкой:{' '}
              <span className="totalPriceDiscount">{formatPrice(this.props.subtotal, 0, 3, ' ', ',')} Р</span>
              {formatPrice(this.props.total, 0, 3, ' ', ',')} Р
            </div>
          ) : (
            <div className="totalPrice">
              <span>Итого:</span>
              <span>{formatPrice(this.props.total, 0, 3, ' ', ',')} Р</span>
            </div>
          )}
          {this.props.percent > 0 && (
            <div className="percentPrice">
              <span>Ваша скидка {this.props.carts.discount.discount.value}%:</span>{' '}
              <span>{formatPrice(this.props.percent, 0, 3, ' ', ',')} Р</span>
            </div>
          )}
          {this.props.order && (
            <div className="percentPrice">
              <span>Доставка:</span>
              <span>{formatPrice(500, 0, 3, ' ', ',')} Р</span>
            </div>
          )}
        </div>
        <Carts
          carts={this.props.carts.cart}
          productIncrament={this.props.productIncrament}
          productDelete={this.props.productDelete}
        />
      </div>
    )
  }
}

function mapStateToProps(store) {
  return {
    carts: store,
    subtotal: subtotalSelector(store),
    percent: taxSelector(store),
    total: totalSelector(store)
  }
}
function mapDispatchToProps(dispatch) {
  return {
    fetchData: url => dispatch(cartFetchData(url)),
    requestCart: () => dispatch(requestCart()),
    productIncrament: item => dispatch(productIncrament(item)),
    productDelete: item => dispatch(productDelete(item))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer)

const formatPrice = (number, n, x, s, c) => {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')'
  const num = number.toFixed(Math.max(0, ~~n))

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','))
}
