import './profile.styl'

// import createStore from './profileCreateStore'
import createStore, { sagaMiddleware } from './profileCreateStore'

import ProfileContainer from './profile/ProfileContainer'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import { cartSaga } from './cart/CartReducer'
import { discountSaga } from './profile/DiscountReducer'
import { profileSaga } from './profile/ProfileReducer'

const store = createStore()

sagaMiddleware.run(cartSaga)
sagaMiddleware.run(profileSaga)
sagaMiddleware.run(discountSaga)

ReactDOM.render(
  <Provider store={store} key="provider">
    <div className="ProfileApp">
      <ProfileContainer />
    </div>
  </Provider>,
  document.getElementById('root')
)
