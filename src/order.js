import './order.styl'

// import createStore from './profileCreateStore'
import createStore, { sagaMiddleware } from './profileCreateStore'

import OrderContainer from './order/OrderContainer'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import { cartSaga } from './cart/CartReducer'

const store = createStore()

sagaMiddleware.run(cartSaga)

ReactDOM.render(
  <Provider store={store} key="provider">
    <div className="OrderApp">
      <OrderContainer />
    </div>
  </Provider>,
  document.getElementById('root')
)
