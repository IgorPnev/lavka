import React, { Component } from 'react'
import Composite from './compontents/composite/composite'

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <style jsx global>{`
        *
            // box-sizing border-box
            -webkit-font-smoothing antialiased
            -webkit-overflow-scrolling touch
            // -webkit-appearance none
        @reset-global pc
        .App
            *
                box-sizing border-box !important
        html
            height 100%
            font-size 16px



        body
            font-family 'proxima-soft', Arial, Helvetica, sans-serif
            -webkit-font-smoothing antialiased`}</style> */}
        <style jsx global>{`
          body
              font-family 'proxima-soft', Arial, Helvetica, sans-serif
              -webkit-font-smoothing antialiased
          .App
              *
                  box-sizing border-box !important
                  &:after
                      box-sizing border-box !important
                  &:before
                      box-sizing border-box !important`}</style>

        <Composite />
      </div>
    )
  }
}

export default App
