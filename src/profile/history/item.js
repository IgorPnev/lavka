import './Item.styl'

import React from 'react'
const HistoryItem = ({ item }) => {
  return (
    <div className="profileHistoryItem">
      <div className="profileHistoryDate">{item.date}</div>
      <div className="profileHistoryAddress">{item.address}</div>
      <div className="profileHistoryTotal">{formatPrice(parseFloat(item.total), 0, 3, ' ', ',')} Р</div>
      <div className="profileHistoryRepeat">
        <svg height="23" width="23">
          <path d="M11.5 22.5V.5m-11 11h22" fill="none" stroke="#979797" strokeLinecap="square" />
        </svg>
      </div>
    </div>
  )
}

export default HistoryItem

const formatPrice = (number, n, x, s, c) => {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')'
  const num = number.toFixed(Math.max(0, ~~n))
  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','))
}
