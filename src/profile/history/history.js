import './History.styl'

import Item from './Item'
import React from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import { connect } from 'react-redux'

const History = ({ history }) => {
  console.log(history)
  return (
    <div className="profileHistory">
      <Scrollbars style={{ width: '100%' }}>
        <div>
          {history &&
            history.map((item, index) => {
              return <Item item={item} key={index} />
            })}
        </div>
      </Scrollbars>
      <div className="buttons">
        <button
          type="button"
          className="buttonSubmit"
          onClick={() => {
            window.history.back()
          }}>
          Вернуться
        </button>
      </div>
    </div>
  )
}

function mapStateToProps(store) {
  return {
    history: store.profile.profile.profile.history
  }
}

export default connect(mapStateToProps, undefined)(History)
