import { call, put, takeEvery } from 'redux-saga/effects'

const DISCOUNT_HAS_ERRORED = 'DISCOUNT_HAS_ERRORED'
const DISCOUNT_FETCH_DATA_SUCCESS = 'DISCOUNT_FETCH_DATA_SUCCESS'
const DISCOUNT_IS_LOADING = 'DISCOUNT_IS_LOADING'
const DISCOUNT_IS_LOAD = 'DISCOUNT_IS_LOAD'
const DISCOUNT_REQUEST = 'DISCOUNT_REQUEST'

const initialState = {
  discount: {
    result: 'NotFound',
    value: 0
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case DISCOUNT_IS_LOAD:
      return { ...state, isLoad: action.isLoad }
    case DISCOUNT_IS_LOADING:
      return { ...state, isLoading: action.isLoading }
    case DISCOUNT_HAS_ERRORED:
      return { ...state, hasErrored: action.hasErrored }
    case DISCOUNT_FETCH_DATA_SUCCESS:
      return { ...state, discount: action.discount }
    default:
      return state
  }
}

export function discountHasErrored(bool) {
  return {
    type: DISCOUNT_HAS_ERRORED,
    hasErrored: bool
  }
}

export function discountIsLoading(bool) {
  return {
    type: DISCOUNT_IS_LOADING,
    isLoading: bool
  }
}

export function discountIsLoad(bool) {
  return {
    type: DISCOUNT_IS_LOAD,
    isLoad: bool
  }
}

export function discountFetchDataSuccess(discount) {
  return {
    type: DISCOUNT_FETCH_DATA_SUCCESS,
    discount: discount
  }
}

// saga

export function requestDiscount(card) {
  return { type: DISCOUNT_REQUEST, card: card }
}

export function* watchFetchDiscount() {
  yield takeEvery(DISCOUNT_REQUEST, fetchDiscountAsync)
}

function* fetchDiscountAsync(action) {
  try {
    yield put(discountIsLoading(true))
    const data = yield call(() => {
      return fetch(`/api/check-card`, {
        method: 'post',
        credentials: 'include',
        body: JSON.stringify({ card: action.card }),
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
    })
    yield put(discountFetchDataSuccess(data))
    yield put(discountIsLoading(false))
    yield put(discountIsLoad(true))
  } catch (error) {
    yield put(discountHasErrored())
  }
}

export function* discountSaga() {
  yield [watchFetchDiscount()]
}
