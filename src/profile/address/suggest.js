import React, { Component } from 'react'

import Autosuggest from 'react-autosuggest'
import css from 'styled-jsx/css'

const getSuggestionValue = suggestion => suggestion.displayName
const renderSuggestion = suggestion => <div>{suggestion.displayName}</div>
const shouldRenderSuggestions = () => true

class Suggest extends Component {
  constructor() {
    super()

    this.state = {
      value: '',
      suggestions: [],
      maps: null
    }
  }
  componentDidMount() {
    this.props.input.value && this.setState({ value: this.props.input.value })
  }
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    })
    this.props.input && this.props.input.onChange(newValue)
  }

  onSuggestionsFetchRequested = ({ value }) => {
    if (window.ymaps) {
      window.ymaps
        .suggest(value, {
          boundedBy: [[59.5, 29.35], [60.32, 30.72]]
        })
        .then(items => {
          this.setState({
            suggestions: value.length === 0 ? [] : items
          })
        })
    } else {
      return []
    }
  }

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  handleApiAvaliable(ymaps) {
    this.setState({ maps: ymaps })
  }

  render() {
    const { value, suggestions } = this.state
    const inputProps = {
      placeholder: 'Введите адрес',
      value,
      onChange: this.onChange
    }

    return (
      <div className="geosearch">
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          shouldRenderSuggestions={shouldRenderSuggestions}
          inputProps={inputProps}
        />

        <style global jsx>
          {suggest}
        </style>
      </div>
    )
  }
}

export default Suggest

const suggest = css`
  .geosearch {
    width: 100%;
    margin-bottom: 20px;
  }
  .react-autosuggest__container {
    position: relative;
  }

  .react-autosuggest__input {
    width: 100%;
    height: 40px;
    padding: 10px 20px;
    color: #4a4a4a;
    font-size: 13px;
    font-weight: 300;
    letter-spacing: 1px;
    border: 1px solid #d0d0d0;
    background-color: transparent;
  }

  .react-autosuggest__input--focused {
    outline: none;
  }

  .react-autosuggest__input--open {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }

  .react-autosuggest__suggestions-container {
    display: none;
  }

  .react-autosuggest__suggestions-container--open {
    display: block;
    position: absolute;
    top: 39px;
    width: 100%;
    border: 1px solid #d0d0d0;
    background-color: #fff9ea;
    color: #4a4a4a;
    font-size: 13px;
    font-weight: 300;
    z-index: 2;
  }

  .react-autosuggest__suggestions-list {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }

  .react-autosuggest__suggestion {
    cursor: pointer;
    padding: 10px 20px;
  }

  .react-autosuggest__suggestion--highlighted {
    background-color: #e9f0e0;
  }
`
