import './address.styl'

import { Field, Form, FormSpy } from 'react-final-form'
import React, { Fragment } from 'react'

import { FieldArray } from 'react-final-form-arrays'
import { Scrollbars } from 'react-custom-scrollbars'
import Suggest from './suggest'
import { YMaps } from 'react-yandex-maps'
import arrayMutators from 'final-form-arrays'
import { connect } from 'react-redux'
import css from 'styled-jsx/css'
import { updateProfile } from './../ProfileReducer'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const required = value => (value ? undefined : 'Required')

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}
const initData = {
  customers: [
    {
      address: 'Зверинская улица, 7-9, Санкт-Петербург'
    }
  ],
  default: '0'
}
const AddressForm = ({ address, updateProfile }) => {
  return (
    <div className="container">
      <Form
        onSubmit={onSubmit}
        mutators={{
          ...arrayMutators
        }}
        initialValues={address}
        render={({
          handleSubmit,
          mutators: { push }, // injected from final-form-arrays above
          pristine,
          submitting,
          values
        }) => {
          return (
            <Fragment>
              <FormSpy
                subscription={{ values: true }}
                onChange={value => {
                  updateProfile(value)
                }}
              />
              <form className="form" onSubmit={handleSubmit}>
                <Scrollbars style={{ width: '100%' }}>
                  <div className="fields">
                    <FieldArray name="address">
                      {({ fields }) =>
                        fields.map((name, index) => (
                          <div key={name} className="formItem">
                            <label className="label">
                              Адрес №{index + 1} <span onClick={() => fields.remove(index)}>Удалить</span>
                            </label>
                            <div className="formItemAddress">
                              <Field name={`${name}.address`} component={Suggest} validate={required} />
                              <Field
                                name={`${name}.flat`}
                                className="inputFlat"
                                component="input"
                                type="text"
                                placeholder="кв/оф"
                              />
                            </div>

                            <label className="radio">
                              <Field name={`default`} component="input" type="radio" value={`${name}.default`} />
                              <div className="radio__text">Сделать адрес адресом по умолчанию</div>
                            </label>
                          </div>
                        ))
                      }
                    </FieldArray>
                  </div>
                </Scrollbars>
                <div className="buttons">
                  <button type="button" className="buttonAdd" onClick={() => push('address', undefined)}>
                    Добавить адрес
                  </button>
                  <button
                    type="button"
                    className="buttonSubmit"
                    onClick={() => {
                      window.history.back()
                    }}>
                    Вернуться
                  </button>
                </div>
              </form>
              {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
            </Fragment>
          )
        }}
      />

      <YMaps />
      <style jsx global>
        {radiostyles}
      </style>
      <style jsx>{styles}</style>
    </div>
  )
}

function mapStateToProps(store) {
  return {
    address: store.profile.profile.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    updateProfile: value => dispatch(updateProfile(value))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddressForm)

const styles = css`
  .formItem {
    margin-bottom: 22px;
  }
  .container {
    align-self: stretch;
    flex: 1 1 auto;
    height: 100%;
    padding: 25px 10px 25px 25px;
  }
  .form {
    flex: 1 1 auto;
    height: 100%;
    box: vertical;
  }
  .fields {
    flex: 1 1 auto;
    position: relative;
    padding-right: 15px;
  }

  .buttons {
    flex: 0 1 auto;
    box: vertical top left;
    padding-right: 15px;
  }
  .buttonSubmit {
    display: block;
    background-color: #e9f0e0;
    color: #4a4a4a;
    font-size: 16px;
    font-weight: 300;
    letter-spacing: 1.23px;
    width: 100%;
    height: 40px;
    min-height: 40px;
    border: 0;
    outline: 0;
    max-height: 99px;
    overflow: hidden;
    transition: all 0.4s;
    &:disabled {
      opacity: 0.5;
      color: #aeaeae;
    }
  }
  .buttonAdd {
    color: #cbdfc3;
    text-decoration: underline;
    cursor: pointer;
    font-size: 14px;
    font-weight: 400;
    transition: 0.4s all;
    background: none;
    border: 0;
    outline: 0;
    margin: 0;
    padding: 0;
    margin-bottom: 15px;
    &:hover {
      color: #cfbf99;
      text-decoration: none;
    }
  }
  .label {
    color: #6b6969;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 0.7px;
    margin-bottom: 7px;
    display: block;
    box: horizontal middle space-between;
    span {
      color: #cbdfc3;
      text-decoration: underline;
      cursor: pointer;
      font-size: 12px;
      font-weight: 400;
      letter-spacing: 0.43px;
      transition: 0.4s all;
      &:hover {
        color: #cfbf99;
        text-decoration: none;
      }
    }
  }
`
const radiostyles = css`
  .radio input {
    position: absolute;
    z-index: -1;
    opacity: 0;
    margin: 0px 0 0 7px;
  }
  .radio__text {
    position: relative;
    padding: 0 0 0 30px;
    cursor: pointer;
    color: #9b9b9b;
    font-size: 12px;
    font-weight: 400;
    letter-spacing: 0.43px;
  }
  .radio__text:before {
    content: '';
    position: absolute;
    top: -3px;
    left: 0;
    width: 20px;
    height: 20px;
    border: 1px solid #d0d0d0;
    background: transparent;
  }
  .radio__text:after {
    content: '';
    position: absolute;
    top: 0px;
    left: 3px;
    width: 14px;
    height: 14px;
    background-repeat: no-repeat;
    background-position: center center;
    opacity: 0;
    transition: 0.2s;
    background-image: url('data:image/svg+xml;charset=utf-8,%3Csvg%20id%3D%22eb398f14-ac70-450a-b756-dd4b418e71c7%22%20data-name%3D%22Layer%201%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2016.23%2010.99%22%3E%3Ctitle%3Eapprove-2%3C%2Ftitle%3E%3Cpolyline%20id%3D%22f2449095-3a4b-46fe-a3b0-0ccb765616de%22%20data-name%3D%22Path-22534%22%20points%3D%220.35%204.63%206.01%2010.29%2015.88%200.35%22%20style%3D%22fill%3Anone%3Bstroke%3A%234a4a4a%22%2F%3E%3C%2Fsvg%3E');
  }
  .radio input:checked + .radio__text:after {
    opacity: 1;
  }

  .radio input:focus + .radio__text:before {
    border: 1px solid #4a4a4a;
  }
`
