import './User.styl'

import { Field, Form, FormSpy } from 'react-final-form'
import React, { Fragment } from 'react'

import Mask from '../../compontents/form/mask/Mask'
import { connect } from 'react-redux'
import { requestDiscount } from './../DiscountReducer'
import { updateProfile } from './../ProfileReducer'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}

const User = ({ requestDiscount, profile, updateProfile }) => {
  return (
    <div className="user">
      {profile.isLoad && (
        <Fragment>
          <Form
            onSubmit={onSubmit}
            initialValues={profile.profile.profile}
            render={({ handleSubmit, submitting, pristine, values }) => (
              <Fragment>
                <FormSpy
                  subscription={{ values: true }}
                  onChange={value => {
                    updateProfile(value)
                  }}
                />
                <form onSubmit={handleSubmit}>
                  <div className="formItem">
                    <label className="label">Имя и фамилия</label>
                    <Field name="name" component="input" type="text" className="input" placeholder="Имя и фамилия" />
                  </div>
                  <div className="formItem">
                    <label className="label">Email</label>
                    <Field
                      name="email"
                      className="input"
                      component="input"
                      type="email"
                      placeholder="example@email.ru"
                    />
                    <label className="checkbox">
                      <Field name="email_subscribe" component="input" type="checkbox" />
                      <div className="checkbox__text">Получать новости от «Счастья»</div>
                    </label>
                  </div>
                  <div className="formItem">
                    <label className="label">Телефон</label>
                    <Field
                      name="phone"
                      className="input"
                      component="input"
                      type="text"
                      placeholder="+7 (812) 123-23-23"
                    />
                    <label className="checkbox">
                      <Field name="sms_subscribe" component="input" type="checkbox" />{' '}
                      <div className="checkbox__text">Получать новости по SMS</div>
                    </label>
                  </div>
                  <div className="formItem">
                    <label className="label">Номер карты Счастья</label>
                    <div className="formItemHalf">
                      {/* <Field name="card" className="input" component="input" placeholder="1712-12" /> */}
                      <Field
                        style={{ maxWidth: '300px' }}
                        name="card"
                        className="input"
                        component={Mask}
                        mask="11111-11"
                        placeholder="1712-12"
                      />

                      <button
                        className="buttonCheck"
                        type="button"
                        style={{ minWidth: '100px' }}
                        disabled={!/([0-9]{5}-[0-9]{2})/g.exec(values.card)}
                        onClick={() => {
                          requestDiscount(values.card)
                        }}>
                        Проверить
                      </button>
                    </div>
                  </div>

                  {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
                </form>
                <div className="buttons">
                  <button
                    type="button"
                    className="buttonSubmit"
                    onClick={() => {
                      window.history.back()
                    }}>
                    Вернуться
                  </button>
                </div>
              </Fragment>
            )}
          />
        </Fragment>
      )}
    </div>
  )
}

function mapStateToProps(store) {
  return {
    profile: store.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestDiscount: card => dispatch(requestDiscount(card)),
    updateProfile: value => dispatch(updateProfile(value))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(User)
