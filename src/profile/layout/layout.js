import React, { Component } from 'react'
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs'

import AddressForm from '../address/address'
import History from '../history/History'
import User from '../user/user'
import css from 'styled-jsx/css'

class Layout extends Component {
  state = {}

  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>ПРОФИЛЬ</Tab>
          <Tab>ИСТОРИЯ</Tab>
          <Tab>АДРЕСА</Tab>
        </TabList>
        <TabPanel>
          <User />
        </TabPanel>
        <TabPanel>
          <History />
        </TabPanel>
        <TabPanel>
          <AddressForm />
        </TabPanel>
        <style jsx global>
          {tabstyles}
        </style>
      </Tabs>
    )
  }
}

export default Layout

const tabstyles = css`
  .react-tabs {
    box: horizontal middle;
    size: auto 100%;
  }
  .react-tabs__tab-list {
    box: vertical middle left;
    height: 50px;
    width: 315px;
    padding: 0;
  }
  .react-tabs__tab {
    box: middle center;
    flex: 0 1 auto;
    display: flex;
    min-height: 50px;
    position: relative;
    padding: 0;
    width: 315px;
    height: 50px;
    padding-top: 2px;
    color: #9b9b9b;
    font-family: Muller;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 0.75px;
    text-transform: uppercase;
    cursor: pointer;
    align-self: stretch;
    border-bottom: 1px solid #dfd3b4;
    background-color: #f6eedb;
    background-image: url();

    &:last-child {
      border-bottom: 1px solid transparent;
    }
  }
  .react-tabs__tab--selected {
    /* border: 1px solid #cbdfb1; */
    background-color: #e9f0e0;
    clip-path: polygon(0% 0%, calc(100% - 10px) 0%, 100% 50%, calc(100% - 10px) 100%, 0% 100%);
    width: 325px;
    padding-right: 10px;
  }
  .react-tabs__tab--selected .tabname {
    position: relative;
    color: #26a648;
  }
  .react-tabs__tab--selected .tabname:after {
    display: block;
    position: absolute;
    bottom: 2px;
    left: 0;
    z-index: 5;
    background-color: #26a648;
    content: '';
    transition: all 0.4s;
    size: 100% 1px;
  }
  .react-tabs__tab--disabled {
    cursor: default;
  }
  .react-tabs__tab:focus {
    outline: none;
  }
  .react-tabs__tab:focus:after {
    position: absolute;
    right: -4px;
    bottom: -5px;
    left: -4px;
    height: 5px;
    background: rgba(255, 255, 255, 0.9);
    content: '';
  }
  .react-tabs__tab-panel {
    flex: 0 1 450px;
    display: none;
    width: 450px;
    height: 600px;
  }
  .react-tabs__tab-panel--selected {
    display: block;
    background-color: #fff9ea;
  }
`
