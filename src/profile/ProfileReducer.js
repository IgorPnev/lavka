import { call, put, takeEvery, throttle } from 'redux-saga/effects'

const PROFILE_HAS_ERRORED = 'PROFILE_HAS_ERRORED'
const PROFILE_FETCH_DATA_SUCCESS = 'PROFILE_FETCH_DATA_SUCCESS'
const PROFILE_IS_LOADING = 'PROFILE_IS_LOADING'
const PROFILE_IS_LOAD = 'PROFILE_IS_LOAD'
const PROFILE_REQUEST = 'PROFILE_REQUEST'
const PROFILE_FORM_UPDATE = 'PROFILE_FORM_UPDATE'
const initialState = {
  profile: {}
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PROFILE_IS_LOAD:
      return { ...state, isLoad: action.isLoad }
    case PROFILE_IS_LOADING:
      return { ...state, isLoading: action.isLoading }
    case PROFILE_HAS_ERRORED:
      return { ...state, hasErrored: action.hasErrored }
    case PROFILE_FETCH_DATA_SUCCESS:
      return { ...state, profile: action.profile }
    case PROFILE_FORM_UPDATE:
      return { ...state, profile: { profile: action.update.values } }
    default:
      return state
  }
}

export function profileHasErrored(bool) {
  return {
    type: PROFILE_HAS_ERRORED,
    hasErrored: bool
  }
}

export function profileIsLoading(bool) {
  return {
    type: PROFILE_IS_LOADING,
    isLoading: bool
  }
}

export function profileIsLoad(bool) {
  return {
    type: PROFILE_IS_LOAD,
    isLoad: bool
  }
}

export function profileFetchDataSuccess(profile) {
  return {
    type: PROFILE_FETCH_DATA_SUCCESS,
    profile: profile
  }
}

// saga

export function requestProfile() {
  return { type: PROFILE_REQUEST }
}

export function updateProfile(values) {
  return { type: PROFILE_FORM_UPDATE, update: values }
}

export function* watchFetchProfile() {
  yield takeEvery(PROFILE_REQUEST, fetchProfileAsync)
  yield throttle(1500, PROFILE_FORM_UPDATE, fetchProfileUpdateAsync)
}

function* fetchProfileAsync() {
  try {
    yield put(profileIsLoading(true))
    const data = yield call(() => {
      return fetch(`/api/profile`, {
        method: 'get',
        credentials: 'include',
        // body: JSON.stringify({ card: action.card }),
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
    })
    yield put(profileFetchDataSuccess(data))
    yield put(profileIsLoading(false))
    yield put(profileIsLoad(true))
  } catch (error) {
    yield put(profileHasErrored())
  }
}

function* fetchProfileUpdateAsync(action) {
  try {
    const data = yield call(() => {
      return fetch(`/api/profile`, {
        method: 'post',
        credentials: 'include',
        body: JSON.stringify(action.update.values),
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
    })
  } catch (error) {
    yield put(profileHasErrored())
  }
}

export function* profileSaga() {
  yield [watchFetchProfile()]
}
