import './ProfileContainer.styl'

import React, { Component } from 'react'

import CartContainer from '../cart/CartContainer.js'
import Layout from './layout/layout'
import { connect } from 'react-redux'
import { hot } from 'react-hot-loader'
import { requestProfile } from './ProfileReducer'

class ProfileContainer extends Component {
  componentDidMount() {
    this.props.requestProfile()
  }
  render() {
    return (
      <div className="profile">
        <div>
          <div className="spacer" style={{ height: '92px' }} />
          <Layout />
        </div>
        <CartContainer />
      </div>
    )
  }
}

function mapStateToProps(store) {
  return {
    carts: store
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestProfile: () => dispatch(requestProfile())
  }
}

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(ProfileContainer))
