import cart from './cart/CartReducer'
import { combineReducers } from 'redux'
import discount from './profile/DiscountReducer'
import profile from './profile/ProfileReducer'

export default combineReducers({
  cart,
  profile,
  discount
})
