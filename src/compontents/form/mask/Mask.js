import './Mask.styl'

import React, { Component } from 'react'

import MaskedInput from 'react-maskedinput'
import classNames from 'classnames'

class Mask extends Component {
  constructor(props) {
    super(props)

    this.state = {
      blurred: false
    }
  }

  handleBlur() {
    this.setState({
      blurred: true
    })
  }

  handleFocus() {
    this.setState({
      blurred: false
    })
  }

  render() {
    let inputClass = classNames({
      input: true
      //   inputError: this.props.showError() && this.state.blurred
    })

    const { input } = this.props

    return (
      <MaskedInput
        type="text"
        mask={this.props.mask}
        className={inputClass}
        placeholder={this.props.placeholder}
        //   onFocus={this.handleFocus.bind(this)}
        //   onBlur={this.handleBlur.bind(this)}
        onChange={event => input.onChange(event.target.value)}
        value={input.value}
      />
    )
  }
}

export default Mask
