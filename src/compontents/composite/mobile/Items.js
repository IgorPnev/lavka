import React from 'react'
import css from 'styled-jsx/css'

import RandomButton from '../drop/RandomButton'
import Item from './Item'

class Items extends React.Component {
  render() {
    return (
      <div>
        <div className="random">
          <RandomButton onClick={() => this.handleRandom()} />
        </div>
        <div className="dropItems">
          {this.props.items.map((item, index) => {
            return <Item key={'item' + index} item={item} onClick={() => this.handleClick(item)} />
          })}
        </div>
        <style jsx>{stylesItems}</style>
      </div>
    )
  }

  handleRandom() {
    const arr = this.props.collection.map(item => {
      item.lastDroppedItem = this.props.items[Math.floor(Math.random() * this.props.items.length)]
      return item
    })

    arr.sort((a, b) => {
      if (a.lastDroppedItem === null) {
        return 1
      } else if (b.lastDroppedItem === null) {
        return -1
      } else if (a.lastDroppedItem.id === b.lastDroppedItem.id) {
        return 0
      }
      return a.lastDroppedItem.id < b.lastDroppedItem.id ? -1 : 1
    })

    this.props.updateCollection(arr)
  }
  handleClick(item) {
    const inx = this.props.collection.findIndex(elem => !elem.lastDroppedItem)
    if (inx !== -1) {
      const arr = this.props.collection
      arr[inx].lastDroppedItem = item
      this.props.updateCollection(arr)
    } else {
      this.setState({ boxFull: true })
    }
  }
}

export default Items

const stylesItems = css`
  .dropItems {
    box: horizontal top left;
    flex-wrap: wrap;
    padding: 0 20px;
  }

  .random {
    margin-bottom: 20px;
    padding: 0 20px;
  }
`
