import React from 'react'
import EasyTransition from 'react-easy-transition'
import Swiper from 'react-id-swiper'
import Scrollchor from 'react-scrollchor'
import Switch, { Case, Default } from 'react-switch-case'
import css from 'styled-jsx/css'

import { NextArrow, PrevArrow } from '../order/BoxSliderArrow'
import BarItem from './BarItem'

const Bar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      clicked: null
    }
    this.updateClicked = this.updateClicked.bind(this)
  }

  updateClicked(state) {
    this.setState({ clicked: state })
  }
  goNext() {
    if (this.swiper) {
      this.swiper.slideNext()
      this.setState({ clicked: null })
    }
  }

  goPrev() {
    if (this.swiper) {
      this.swiper.slidePrev()
      this.setState({ clicked: null })
    }
  }

  handleRemove(index) {
    let arr = this.props.collection
    this.props.collection[index].lastDroppedItem = null
    this.props.updateCollection(arr)
    this.props.updateBasket('composite')
  }

  render() {
    const params = {
      slidesPerView: 'auto',
      freeMode: true,
      on: {
        touchMove: () => {
          this.setState({ clicked: null })
        }
      }
    }

    return (
      <div className="bar">
        <div className="dropCount">
          {countDrops(this.props.collection)} / {this.props.size}
        </div>
        <div className="slider">
          <PrevArrow onClick={() => this.goPrev()} />
          <NextArrow onClick={() => this.goNext()} />
          <Swiper
            {...params}
            style={{ height: '60px' }}
            ref={node => {
              if (node) this.swiper = node.swiper
            }}>
            {this.props.collection.map(({ lastDroppedItem }, index) => (
              <BarItem
                key={'bar' + index}
                lastDroppedItem={lastDroppedItem}
                isClicked={this.state.clicked}
                remove={() => this.handleRemove(index)}
                updateClicked={this.updateClicked}
                index={index}
              />
            ))}
          </Swiper>
        </div>

        <Switch condition={this.props.stateBasket}>
          <Case value="composite">
            <Scrollchor className="add" to="#top" animate={{ offset: 20, duration: 600 }}>
              <button key="exp1" className="add">
                Собирите набор
              </button>
            </Scrollchor>
          </Case>
          <Case value="pack">
            <Scrollchor
              afterAnimate={() => this.props.updateBasket('add')}
              className="add"
              to="#compositeBottom"
              animate={{ offset: 20, duration: 600 }}>
              <button key="exp2" className="add">
                <EasyTransition
                  path={window.location.pathname}
                  initialStyle={{ width: '100vw', opacity: 0, transform: 'scale(0)' }}
                  transition="all 0.3s ease-in"
                  finalStyle={{ opacity: 1, transform: 'scale(1)' }}
                  leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
                  Выберите упаковку
                </EasyTransition>
              </button>
            </Scrollchor>
          </Case>
          <Case value="add">
            <button key="exp3" onClick={() => this.props.addBasket()} className="add">
              <EasyTransition
                path={window.location.pathname}
                initialStyle={{ width: '100vw', opacity: 0, transform: 'scale(0)' }}
                transition="all 0.3s ease-in"
                finalStyle={{ opacity: 1, transform: 'scale(1)' }}
                leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
                Добавить в корзину
              </EasyTransition>
            </button>
          </Case>
          <Case value="added">
            <button className="add">
              <EasyTransition
                path={window.location.pathname}
                initialStyle={{ width: '100vw', opacity: 0, transform: 'scale(0)' }}
                transition="all 0.3s ease-in"
                finalStyle={{ opacity: 1, transform: 'scale(1)' }}
                leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
                Добавлено
              </EasyTransition>
            </button>
          </Case>
          <Case value="cart">
            <a href="/cart" className="add">
              <EasyTransition
                path={window.location.pathname}
                initialStyle={{ width: '100vw', opacity: 0, transform: 'scale(0)', textAlign: 'center' }}
                transition="all 0.3s ease-in"
                finalStyle={{ opacity: 1, transform: 'scale(1)' }}
                leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
                Перейти в корзину
              </EasyTransition>
            </a>
          </Case>
          <Default>
            Errr!
            <strong>default state</strong>
          </Default>
        </Switch>

        <style jsx>{styles}</style>
        <style global jsx>
          {stylesButton}
        </style>
      </div>
    )
  }
}

export default Bar

const styles = css`
  .bar {
    display: block;
    position: fixed;
    bottom: 0px;
    size: 100vw 126px;
    z-index: 1000;
  }
  .slider {
    position: relative;
  }
  .dropCount {
    color: rgba(246, 238, 219, 1);
    font-size: 14px;
    letter-spacing: 0;
    background-color: rgba(223, 211, 180, 1);
    padding: 4px 8px;
    border-radius: 0 3px 0 0;
    display: inline-block;
    width: 59px;
    text-align: center;
    margin-bottom: -1px;
    z-index: 1001;
  }
`
const stylesButton = css`
  .add {
    box: vertical middle center;
    width: 100%;
    height: 44px;
    background: #e9f0e0;
    border: 0;
    color: #4a4a4a;
    font-size: 16px;
    font-weight: 300;
    letter-spacing: 1.23px;
    outline: 0;
    border: 1px solid #cbdfb1;
    margin-top: -1px;
    text-decoration: none;
  }
`

const countDrops = arr => {
  const empties = arr.reduce((x, y) => {
    if (y.lastDroppedItem === null) {
      return x - 1
    }
    return x
  }, arr.length)
  return empties
}

// const buttonMessage = state => {
//   switch (state) {
//     case 'composite':
//       return {
//         __html: `<button>
//       Собирите набор
//       </button>`
//       }
//       break

//     case 'pack':
//       return 'Выберите упаковку'
//       break
//     case 'add':
//       return 'Добавить в корзину'
//       break
//     case 'added':
//       return 'Добавлено'
//       break

//     default:
//       return 'Собирите набор'
//       break
//   }
// }
