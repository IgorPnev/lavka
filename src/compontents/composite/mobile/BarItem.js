import cx from 'classnames'
// import { update } from 'immutability-helper'
import React, { Component } from 'react'
import EasyTransition from 'react-easy-transition'
import css from 'styled-jsx/css'

class BarItem extends Component {
  state = {
    click: false
  }

  handleClicked() {
    if (!this.props.isClicked === this.props.index || !this.props.isClicked) {
      this.props.updateClicked(this.props.index)
      if (this.props.lastDroppedItem) {
        this.setState({ click: true })
      } else {
        this.props.updateClicked(null)
      }
    } else {
      this.props.updateClicked(null)
    }

    if (this.props.isClicked === this.props.index) {
      if (this.state.click) {
        this.props.remove(this.props.index)
        this.props.updateClicked(null)
      }
    }
  }

  render() {
    const classes = cx({
      dropBox: true,
      'swiper-slide': true,
      dropBoxActive: this.props.lastDroppedItem && this.props.isClicked === this.props.index
    })

    return (
      <div className={classes} onClick={() => this.handleClicked()}>
        <EasyTransition
          path={window.location.pathname}
          initialStyle={{ opacity: 0, transform: 'scale(0)' }}
          transition="all 0.3s ease-in"
          finalStyle={{ opacity: 1, transform: 'scale(1)' }}
          leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
          {this.props.lastDroppedItem && (
            <img src={'http://lavka.monkeylab.ru/' + this.props.lastDroppedItem.image} alt="ss" />
          )}
        </EasyTransition>
        <div className="dropBoxRemove">
          <div className="dropBoxRemoveIcon">
            <svg height="32" width="32" viewBox="0 0 32 32">
              <g fill="none">
                <path d="M16 0C7.2 0 0 7.2 0 16s7.2 16 16 16 16-7.2 16-16S24.8 0 16 0z" fill="#FFF9EA" />
                <path d="M20.72 11L11 20.71M20.72 20.71L11 11" stroke="#979797" strokeLinecap="round" />
              </g>
            </svg>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    )
  }
}

export default BarItem

const styles = css`
  .dropBox {
    box: vertical middle center;
    flex: 1 1 60px;
    max-width: 60px;
    min-width: 60px;
    margin-left: -1px;
    border: 1px solid #efe5ce;
    border-radius: 5px 5px 0 0;
    size: 60px;
    position: relative;
    background-color: #fff9ea;

    &:first-child {
      border-radius: 0;
    }
    &:nth-child(2) {
      border-radius: 0 5px 0 0;
    }
  }
  .dropBoxActive {
    .dropBoxRemove {
      opacity: 1;
      visibility: visible;
    }
  }
  .dropBox img {
    width: 100%;
    object-fit: cover;
  }
  .dropBoxRemove {
    position: absolute;
    size: 100%;
    top: 0;
    left: 0;
    opacity: 0;
    visibility: hidden;
    transition: all 0.4s;
    &:after {
      display: block;
      position: absolute;
      content: '';
      size: 52px;
      top: 2px;
      left: 2px;
      opacity: 0.43;
      background: #4a4a4a;
      border-radius: 5px;
      z-index: 10;
    }
  }

  .dropBoxRemoveIcon {
    opacity: 1;
    position: absolute;
    z-index: 11;
    left: center;
    top: center;
    svg {
      size: ;
    }
  }
`
