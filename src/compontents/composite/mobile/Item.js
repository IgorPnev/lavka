import React from 'react'
import css from 'styled-jsx/css'

class Item extends React.Component {
  state = {}
  render() {
    return (
      <div className="dropItem" onClick={this.props.onClick}>
        <img src={'http://lavka.monkeylab.ru/' + this.props.item.image} alt="ss" />
        <p>{this.props.item.name}</p>
        <style jsx>{styles}</style>
      </div>
    )
  }
}

export default Item

const styles = css`
  .dropItem {
    box: vertical top center;
    width: 95px;
    margin-right: 20px;
    margin-bottom: 5px;
    lost-column: 1/3 3 20px flex;
    &:active {
      img {
        transform scale(0.9);
      }
    }
    img {
      margin-bottom: 0;
      size: 75px;
      object-fit: contain;
      transition: all .2s easeInOutCubic;
    }
    p {
      color: #4a4a4a;
      font-size: 13px;
      line-height: 15px;
      letter-spacing: 0.41px;
      text-align: center;
      font-width: 300;
    }
  }
`
