import AddButton from './AddButton'
import AnimatedNumber from 'react-animated-number'
import BoxSelect from './BoxSelect'
import { BoxSlider } from './BoxSlider'
import MediaQuery from 'react-responsive'
import QtySelect from './QtySelect'
// import './Order.styl'
import React from 'react'
import Waypoint from 'react-waypoint'
import css from 'styled-jsx/css'

class Order extends React.Component {
  state = {
    load: false
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handlerLoad.bind(this))
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handlerLoad.bind(this))
  }
  handlerLoad() {
    this.setState({ load: true })
  }
  render() {
    const product = this.props.product
    const category = this.props.category

    // const tags = trasformTags(this.props.product, this.props.currentBox)
    const tags = this.props.tags

    return (
      <div className="compositeBottom" id="compositeBottom">
        <MediaQuery query="(max-width: 1024px)">
          <div className="compositeOrderHeader" style={{ textAlign: 'center', marginBottom: '20px' }}>
            Выберите упаковку
          </div>
          <div className="compositeBoxSlider">
            <BoxSlider slide={this.props.currentBox} slides={product} updateBox={this.props.updateBox} />
          </div>
          <Waypoint
            onEnter={() => {
              this.state.load && this.props.stateBasket === 'pack' && this.props.updateBasket('add')
            }}
          />
        </MediaQuery>
        <div className="compositeOrder">
          <div className="compositeOrderColumn compositeOrderColumnLeft">
            <div className="compositeOrderHeader">{category.name}</div>
            <div className="compositeOrderContent">
              <div className="compositeOrderDescription">
                <p>{category.description}</p>
                <div className="compositeOrderIngredients">
                  <span>
                    <b>Состав:</b>
                  </span>
                  {tags &&
                    tags.map((item, index) => {
                      return (
                        <div className="compositeOrderIngredient" key={'ingredients' + index}>
                          #{item}
                        </div>
                      )
                    })}
                </div>
                <div className="compositeOrderStoring">
                  <b>Сроки хранения:</b> {category.storing}
                </div>
                <div className="compositeOrderDelivering">
                  <b>Условия доставки:</b> {category.delivering}
                </div>
              </div>
            </div>
          </div>
          <div className="compositeOrderColumn compositeOrderColumnPack">
            <MediaQuery query="(min-width: 1025px)">
              <div className="compositeOrderHeader compositeOrderHeaderCenter">Выберите упаковку</div>
            </MediaQuery>

            <div className="compositeOrderContent">
              <BoxSelect boxes={product} updateBox={this.props.updateBox} currentBox={this.props.currentBox} />
            </div>
          </div>
          <div className="compositeOrderColumn compositeOrderColumnRight">
            <div className="compositeOrderHeader">Ваш заказ</div>
            <div className="compositeOrderContent">
              <QtySelect updateQty={this.props.updateQty} currentQty={this.props.currentQty} />
              <MediaQuery query="(min-width: 1025px)">
                <AddButton />
              </MediaQuery>

              <div className="compositeOrderPrice">
                <span>Итоговая сумма:</span>
                <div>
                  {/* {formatPrice(this.props.finalPrice, 0, 3, ' ', ',')}&nbsp;₽ */}
                  <AnimatedNumber
                    component="div"
                    value={this.props.finalPrice}
                    style={{
                      transition: '0.8s ease-out',
                      transitionProperty: 'background-color, color, opacity',
                      whiteSpace: 'no-wrap'
                    }}
                    frameStyle={perc => (perc === 100 ? {} : { opacity: 0.25 })}
                    duration={300}
                    formatValue={n => formatPrice(n, 0, 3, ' ', ',')}
                  />&nbsp;₽
                </div>
              </div>
              <MediaQuery query="(min-width: 1025px)">
                <button
                  disabled={this.props.stateBasket === 'pack' ? false : true}
                  onClick={() => this.props.addBasket()}
                  className="compositeOrderBasketAdd">
                  {this.props.stateBasket === 'cart' ? 'Добавлено' : 'Добавить в корзину'}
                </button>
              </MediaQuery>
            </div>
          </div>
        </div>
        <MediaQuery query="(min-width: 1025px)">
          <div className="compositeBoxSlider">
            <BoxSlider slide={this.props.currentBox} slides={product} updateBox={this.props.updateBox} />
          </div>
        </MediaQuery>

        <style jsx>{styles}</style>
      </div>
    )
  }
}

export default Order

const formatPrice = (number, n, x, s, c) => {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')'
  const num = number.toFixed(Math.max(0, ~~n))

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','))
}

const styles = css`.compositeBottom
    padding 50px 0

.compositeOrder
    box horizontal
    max-width 1440px
    margin 0 auto
    margin-bottom 40px
    padding 0 25px
    @media(max-width: 1024px)
        box vertical top
        margin-bottom 80px

.compositeOrderColumn
    lost-column 1 / 3 3 40px flex
    box vertical top left
    position relative
    padding 5px 20px 20px
    @media(max-width: 1024px)
        lost-column 1 / 1 1 0px flex
        padding 5px 0px 20px
        &:after
            display none
    &:after
        position absolute
        top 0
        right -30px
        background-color #9b9b9b
        content ''
        size 1px 100%
    &:first-child
        padding-left 0
    &:last-child
        padding-right 0
        &:after
            display none
.compositeOrderColumnPack
    @media(max-width: 1024px)
        order 0

.compositeOrderColumnLeft
    @media(max-width: 1024px)
        order 3

.compositeOrderColumnRight
    @media(max-width: 1024px)
        order 2

.compositeOrderHeader
    flex 0 1 auto
    width 100%
    margin-bottom 10px
    color #4A4A4A
    font-size 20px
    font-weight 300
    letter-spacing .8px
    text-transform uppercase
    &Center
        text-align center

.compositeOrderContent
    box vertical top left
    flex 1 1 auto
    align-self stretch
    p
        position relative
        margin-bottom 10px
        padding-bottom 10px
        color #4A4A4A
        font-size 14px
        line-height 20px
        letter-spacing .5px
        &:after
            position absolute
            bottom 0
            left 0
            background-color #EFE5CE
            content ''
            size 70px 1px

.compositeOrderIngredients
    position relative
    margin-bottom 10px
    padding-bottom 10px
    span
        display block
        color #292b2c
        font-size 14px
        line-height 20px
        letter-spacing .5px
    &:after
        position absolute
        bottom 0
        left 0
        background-color #EFE5CE
        content ''
        size 70px 1px

.compositeOrderIngredient
    display inline-block
    margin-right 10px
    color #9b9b9b
    font-size 14px
    line-height 20px
    letter-spacing .5px
    text-decoration none

.compositeOrderStoring
    position relative
    margin-bottom 10px
    padding-bottom 10px
    color #4A4A4A
    font-size 14px
    line-height 20px
    letter-spacing .5px
    &:after
        position absolute
        bottom 0
        left 0
        background-color #EFE5CE
        content ''
        size 70px 1px

.compositeOrderDelivering
    position relative
    margin-bottom 10px
    padding-bottom 10px
    color #4A4A4A
    font-size 14px
    line-height 20px
    letter-spacing .5px

.compositeOrderPrice
    box horizontal middle space-between
    width 100%
    margin-bottom 20px
    padding-top 16px
    border-top 1px solid #EFE5CE
    color #4A4A4A
    font-size 20px
    font-weight 300
    letter-spacing .8px
    div
        box horizontal middle left
.compositeOrderBasketAdd
    box vertical middle center
    width 100%
    height 44px
    background #E9F0E0
    border 0
    color #4A4A4A
    font-size 16px
    font-weight 300
    letter-spacing 1.23px
    outline 0
    &:disabled
      opacity .5`
