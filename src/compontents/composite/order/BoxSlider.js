import React from 'react'

import Slider from 'react-slick'
import css from 'styled-jsx/css'

import { NextArrow, PrevArrow } from './BoxSliderArrow'

export class BoxSlider extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.slide) {
      const idx = this.props.slides.findIndex(elem => elem.id === nextProps.slide)
      this.refs.slider.slickGoTo(idx)
    }
  }
  handlePrev() {
    this.refs.slider.slickPrev()
  }

  handleNext() {
    this.refs.slider.slickNext()
  }

  render() {
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: false,
      afterChange: currentSlide => {
        this.props.updateBox(this.props.slides[currentSlide].id)
      },
      ...this.props
    }
    const slides = this.props.slides
    return (
      <div className="boxSlider">
        <PrevArrow onClick={() => this.handlePrev()} />
        <NextArrow onClick={() => this.handleNext()} />

        <Slider ref="slider" {...settings}>
          {slides.map((slide, index) => {
            return (
              <div
                key={'slide' + index}
                className="boxSlide"
                style={{ backgroundImage: 'url(http://lavka.monkeylab.ru/' + slide.banner + ')' }}
              />
            )
          })}
        </Slider>

        <style jsx global>
          {styles}
        </style>
      </div>
    )
  }
}

export default BoxSlider

const styles = css`.slick-slider
            display block
            position relative
            box-sizing border-box
            user-select none
            touch-action pan-y

        .slick-list
            display block
            position relative
            overflow hidden
            margin 0
            padding 0
            &:focus
                outline none
            &.dragging
                cursor pointer
                cursor hand

        .slick-slider .slick-track,
        .slick-slider .slick-list
            transform translate3d(0, 0, 0)

        .slick-track
            display block
            position relative
            top 0
            left 0
            margin-right auto
            margin-left auto
            transition all .4s ease
            &:after
                clear both

        .slick-track:before,
        .slick-track:after
            display table
            content ''

        .slick-loading
            .slick-track
                visibility hidden
            .slick-slide
                visibility hidden

        .slick-slide
            display none
            float left
            height 100%
            min-height 1px
            img
                display block
            &.slick-loading
                img
                    display none
            &.dragging
                img
                    pointer-events none

        [dir='rtl']
            .slick-slide
                float right

        .slick-initialized
            .slick-slide
                display block

        .slick-vertical
            .slick-slide
                display block
                height auto
                border 1px solid transparent

        .slick-arrow
            &.slick-hidden
                display none

        .boxSlider
            display block
            position relative
            width 100%
            overflow hidden
            height 420px
            @media(max-width: 1024px)
                height 150px
                margin-bottom 20px
        .boxSlide
            display block
            width 100%
            height 420px
            background-size cover
            background-repeat no-repeat
            @media(max-width: 1024px)
                height 150px`
