import React from 'react'
import css from 'styled-jsx/css'

const styles = css`
.boxSelectItem
    box vertical top center
    min-width 90px
    position relative
    lost-column 1/3 flex
    margin-bottom 100px
    cursor pointer
    -webkit-tap-highlight-color rgba(0, 0, 0, 0)

    &:last-child
        margin-right 0
    &:after
        display block
        position absolute
        left -12px
        top -12px
        content ''
        size calc(100% + 16px)
        border 4px solid transparent
        border-radius 50%
        box-sizing border-box
    &Active
        .boxSelectItemImage
            transform scale(1.1)
            &:after
                display block
                position absolute
                left -12px
                top -12px
                content ''
                size 90px
                border 4px solid #CBDFB1
                border-radius 50%
        .boxSelectItemName
            color #000
.boxSelectItemImageWrap
    display block
    position relative
    size 80px
    margin-bottom 20px

.boxSelectItemImage
    position absolute
    // margin-bottom 15px
    border 4px solid transparent
    border-radius 50%
    transition all .4s
    size 74px
    max-width 74px
    max-height 74px
    position relative

.boxSelectItemName
    position relative
    margin-bottom 10px
    padding-bottom 3px
    color lighten(#000000, 30%)
    font-size 12px
    font-weight 300
    letter-spacing .58px
    text-transform uppercase
    text-align center
    &:after
        position absolute
        bottom -3px
        left center
        background-color #666
        content ''
        size 50px 1px

.boxSelectItemPrice
    position relative
    color #666666
    font-size 12px
    letter-spacing 1.76px`

const Box = ({ box, active, updateBox }) => {
  return (
    <div className={active ? 'boxSelectItem boxSelectItemActive' : 'boxSelectItem'} onClick={() => updateBox(box.id)}>
      <div className="boxSelectItemImageWrap">
        <div
          className="boxSelectItemImage"
          style={{ backgroundImage: 'url(http://lavka.monkeylab.ru/' + box.image + ')' }}
        />
      </div>
      <div className="boxSelectItemName">{box.name}</div>
      <div className="boxSelectItemPrice">{formatPrice(parseFloat(box.price), 0, 3, ' ', ',')}&nbsp;₽</div>
      <style jsx>{styles}</style>
    </div>
  )
}

export default Box

const formatPrice = (number, n, x, s, c) => {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')'
  const num = number.toFixed(Math.max(0, ~~n))

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','))
}
