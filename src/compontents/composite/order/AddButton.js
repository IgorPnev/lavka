import React from 'react'
const AddButton = props => {
  return (
    <a href="/" className="butt" onClick={props.onClick}>
      <div className="icon">
        <svg height="12" width="12" viewBox="0 0 12 12">
          <g fill="none" stroke="#979797">
            <path d="M12 6H0M6 0v12" />
          </g>
        </svg>
      </div>
      <span>Собрать еще один набор</span>
      <style jsx>{`
        .butt {
          padding: 0;
          background-color: transparent;
          border: 0;
          color: #cfbf99;
          font-size: 14px;
          letter-spacing: 0;
          margin-bottom: 20px;
          outline: 0;
          cursor: pointer;
          transition: all 0.4s;
          box: horizontal middle left;
          text-decoration: none;
          &:hover {
            span {
              border-color: #cfbf99;
              color: #cfbf99;
            }
            .icon {
              background-color: #cfbf99;
            }
          }
        }
        span {
          border-bottom: 1px solid #cbdfc3;
          color: #cbdfc3;
          letter-spacing: 0.3px;
          transition: all 0.4s;
        }
        .icon {
          size: 30px;
          background: #e9f0e0;
          border-radius: 50%;
          margin-right: 10px;
          box: vertical middle center;
          transition: all 0.4s;
        }
      `}</style>
    </a>
  )
}

export default AddButton
