import React from 'react'
import css from 'styled-jsx/css'
// import styles from "./boxselect.styl"

import Box from './Box'

const styles = css`.boxSelect
    box horizontal middle left
    flex 1 1 auto
    flex-wrap wrap
    height 100%
    width 100%
    align-self stretch
    @media(max-width: 1024px)
        box horizontal middle center
        padding-top 30px`

const BoxSelect = ({ boxes, updateBox, currentBox }) => {
  return (
    <div className="boxSelect">
      {boxes.map((box, index) => {
        return <Box box={box} key={'box' + index} active={currentBox === box.id} updateBox={updateBox} />
      })}
      <style jsx>{styles}</style>
    </div>
  )
}

export default BoxSelect
