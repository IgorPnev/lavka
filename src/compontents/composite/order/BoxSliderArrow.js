import React from 'react'

export const PrevArrow = props => {
  const { style, onClick } = props
  return (
    <div className="prev" onClick={onClick}>
      <style jsx>{`
        .prev {
          display: block;
          position: absolute;
          top: calc(50% - 34px);
          left: 0;
          background-color: #fff9ea;
          size: 68px;
          border-radius: 0 100% 100% 0;
          transform: translateX(-34px);
          box: vertical middle right;
          padding-right: 16px;
          z-index: 1000;
          @media (max-width: 1024px) {
            size: 36px;
            transform: translateX(-16px);
            padding-right: 8px;
            padding-bottom: 5px;
            top: calc(50% - 18px);
            svg {
              height: 18px;
            }
          }
        }
      `}</style>
      <svg className="svg" height="20" width="10" viewBox="0 0 20 10">
        <path d="M10 19L0 9.982 9.959 1" fill="none" stroke="#4A4A4A" strokeLinecap="round" />
      </svg>
    </div>
  )
}

export const NextArrow = props => {
  const { style, onClick } = props
  return (
    <div className="next" onClick={onClick}>
      <style jsx>{`
        .next {
          display: block;
          position: absolute;
          top: calc(50% - 34px);
          right: 0;
          background-color: #fff9ea;
          size: 68px;
          border-radius: 100% 0 0 100%;
          transform: translateX(34px);
          box: vertical middle left;
          padding-left: 16px;
          z-index: 1000;
          @media (max-width: 1024px) {
            size: 36px;
            transform: translateX(16px);
            padding-left: 12px;
            padding-bottom: 5px;
            top: calc(50% - 18px);
            svg {
              height: 18px;
            }
          }
        }
      `}</style>
      <svg height="20" width="10" viewBox="0 0 20 10">
        <path d="M0 1l10 9.018L.041 19" fill="none" stroke="#4A4A4A" strokeLinecap="round" />
      </svg>
    </div>
  )
}
