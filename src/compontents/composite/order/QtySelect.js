import React, { Component } from 'react'
// import './QtySelect.styl'
import AnimatedNumber from 'react-animated-number'

class QtySelect extends Component {
  state = {}
  render() {
    return (
      <div className="qtySelect">
        <div className="qtySelectLabel">Количество коробок</div>
        <div className="qtySelectNumbers">
          <button className="qtySelectNumbersMinus" onClick={() => this.handleMinus()}>
            <svg height="23" width="12" viewBox="0 0 12 23">
              <path
                d="M10.507 1.007l-10.1 10.2c-.2.2-.2.4-.2.6 0 .2.1.4.2.6l10.1 10.2c.3.3.8.3 1.1 0 .3-.3.3-.8 0-1.1l-9.6-9.7 9.6-9.6c.3-.3.3-.8 0-1.1-.3-.4-.8-.4-1.1-.1z"
                fill="#3C3C3B"
              />
            </svg>
          </button>
          <div className="qtySelectNumbersQty">
            <AnimatedNumber
              component="div"
              value={this.props.currentQty}
              style={{
                transition: '0.8s ease-out',
                transitionProperty: 'background-color, color, opacity'
              }}
              frameStyle={perc => (perc === 100 ? {} : { opacity: 0.25 })}
              duration={300}
              formatValue={n => Math.trunc(n)}
            />
            {/* {parseFloat(this.props.currentQty)} */}
          </div>

          <button className="qtySelectNumbersPlus" onClick={() => this.handlePlus()}>
            <svg height="23" width="12" viewBox="0 0 12 23">
              <path
                d="M1.493 21.993l10.1-10.2c.2-.2.2-.4.2-.6 0-.2-.1-.4-.2-.6L1.493.393c-.3-.3-.8-.3-1.1 0-.3.3-.3.8 0 1.1l9.6 9.7-9.6 9.6c-.3.3-.3.8 0 1.1.3.4.8.4 1.1.1z"
                fill="#3C3C3B"
              />
            </svg>
          </button>
        </div>
        <style jsx>{`
          .qtySelect {
            box: horizontal middle space-between;
            width: 100%;
            margin-bottom: 30px;
            padding-top: 10px;
          }
          .qtySelectLabel {
            color: #000;
            font-size: 16px;
            font-weight: 300;
            letter-spacing: 0.5px;
          }
          .qtySelectNumbers {
            box: horizontal middle;
          }
          .qtySelectNumbersMinus,
          .qtySelectNumbersPlus {
            display: block;
            margin: 0;
            padding: 0;
            background-color: transparent;
            border: 0;
            size: 12px 23px;
          }
          .qtySelectNumbersMinus:focus,
          .qtySelectNumbersPlus:focus {
            outline: 0;
          }
          .qtySelectNumbersQty {
            width: 60px;
            padding: 0;
            background-color: transparent;
            border-width: 0;
            font-size: 20px;
            font-weight: 200;
            line-height: 1;
            text-align: center;
            cursor: default;
          }
        `}</style>
      </div>
    )
  }
  handleMinus() {
    if (this.props.currentQty > 1) this.props.updateQty(this.props.currentQty - 1)
  }
  handlePlus() {
    this.props.updateQty(this.props.currentQty + 1)
  }
}

export default QtySelect
