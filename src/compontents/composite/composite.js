import MultiBackend, { Preview } from 'react-dnd-multi-backend'

import Bar from './mobile/Bar'
import { DragDropContext } from 'react-dnd'
import Drop from './drop/Drop'
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch'
import ItemTypes from './drop/ItemTypes'
import Items from './mobile/Items'
import MediaQuery from 'react-responsive'
import Order from './order/Order'
import React from 'react'
import axios from 'axios'
import objectAssign from 'object-assign'

class Composite extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      // product: null,
      collection: [],
      box: null,
      finalPrice: null,
      inBasket: false,
      stateBasket: 'composite',
      category: {},
      items: [],
      boxes: [],
      size: 24
    }
    this.updateCollection = this.updateCollection.bind(this)
    this.updateBasket = this.updateBasket.bind(this)
    this.updateBox = this.updateBox.bind(this)
    this.updateQty = this.updateQty.bind(this)
    this.addBasket = this.addBasket.bind(this)
  }
  componentDidMount() {
    this.setState({
      size: window.start.size,
      collection: createCollectionArray(window.start.size),
      tags: []
    })

    // sendPost("https://lavka.monkeylab.ru/api/category_items", {
    //   category: [`${window.start.items}`]
    // })
    //   .then(response => response.json())
    //   .then(data => {
    //     console.log(data)
    //     // this.setState({
    //     //   items: normalizeItems(data.category[0].products)
    //     })

    sendPost('https://lavka.monkeylab.ru/api/category_items', {
      category: [`${window.start.category}`]
    })
      .then(response => response.json())

      .then(data => {
        console.log(data)
        const category = data.category[0].category
        const itemsData = data.category[0].items

        this.setState({
          boxes: normalizeItems(category),
          box: category[0].id,
          qty: 1,
          finalPrice: getPrice(category[0].price, this.state.qty),
          category: category[0],
          items: normalizeItems(itemsData),
          columns: data.category[0].columns,
          rows: data.category[0].rows,
          title: data.category[0].title,
          tags: data.category[0].tags
        })
      })
  }

  generatePreview(type, item, style) {
    objectAssign(style, { zIndex: 10000, width: '50px', height: '50px' })
    return <div style={style}>{/* <img alt="sss" src={"http://lavka.monkeylab.ru/" + item.imageDrag} /> */}</div>
  }

  updateBasket(states) {
    this.setState({
      stateBasket: states
    })
  }
  updateCollection(arr) {
    this.setState({
      collection: arr
    })

    if (this.state.collection.filter(item => item.lastDroppedItem != null).length === parseFloat(this.state.size)) {
      this.updateBasket('pack')
    }
  }
  updateBox(boxselect) {
    const idx = this.state.boxes.findIndex(elem => elem.id === boxselect)
    this.setState({
      box: boxselect,
      finalPrice: getPrice(this.state.boxes[idx].price, this.state.qty)
    })
  }
  updateQty(qty) {
    const idx = this.state.boxes.findIndex(elem => elem.id === this.state.box)
    this.setState({
      qty: qty,
      finalPrice: getPrice(this.state.boxes[idx].price, qty)
    })
  }

  addBasket() {
    const basket = {
      composition: this.state.collection.map(item => item.lastDroppedItem.id),
      box: this.state.box,
      volume: 1,
      length: this.state.qty
    }

    const formData = {
      options: {
        composition: basket.composition,
        box: basket.box,
        volume: basket.volume,
        length: basket.length
      },
      count: basket.length,
      id: basket.box
    }

    axios
      .post('/api/add-cart', JSON.stringify(formData), {
        headers: { Accept: 'application/json, text/plain, */*', 'Content-Type': 'application/json' }
      })
      .then(response => {
        console.log('saved successfully')
        updateHeader()

        this.setState({
          inBasket: true,
          stateBasket: 'added'
        })
        setTimeout(() => {
          this.setState({
            stateBasket: 'cart'
          })
        }, 1000)
      })
  }

  render() {
    return (
      <div className="composite">
        <style jsx>
          {`.composite
            box vertical top
            display block
            height 100%
            min-height 100vh
            padding-top 30px
            background-color #fff9ea

        .compositeHeader
            margin-bottom 30px
            color #4A4A4A
            padding 0 20px
            font-size 20px
            font-weight 300
            letter-spacing .8px
            text-align center
            text-transform uppercase`}
        </style>
        {this.state.boxes &&
          this.state.items &&
          this.state.finalPrice && (
            <div id="top">
              <h1 className="compositeHeader">
                {this.state.title}
                {/* СОБЕРИ СВОЙ НАБОР из {this.state.collection.length > 1
                  ? this.state.collection.length
                  : this.state.size}{" "}
                {this.state.category.name}{" "} */}
              </h1>
              <MediaQuery query="(max-width: 1024px)">
                <Bar
                  size={this.state.size}
                  collection={this.state.collection}
                  updateCollection={this.updateCollection}
                  updateBasket={this.updateBasket}
                  addBasket={this.addBasket}
                  stateBasket={this.state.stateBasket}
                />
                <Items
                  items={this.state.items}
                  collection={this.state.collection}
                  updateCollection={this.updateCollection}
                />
              </MediaQuery>

              <MediaQuery query="(min-width: 1025px)">
                <Drop
                  items={this.state.items}
                  updateCollection={this.updateCollection}
                  size={this.state.size}
                  collection={this.state.collection}
                  columns={this.state.columns}
                  rows={this.state.rows}
                />
              </MediaQuery>

              <Order
                category={this.state.category}
                product={this.state.boxes}
                updateBox={this.updateBox}
                currentBox={this.state.box}
                finalPrice={this.state.finalPrice}
                updateQty={this.updateQty}
                currentQty={this.state.qty}
                addBasket={this.addBasket}
                inBasket={this.state.inBasket}
                size={this.state.size}
                updateBasket={this.updateBasket}
                stateBasket={this.state.stateBasket}
                tags={this.state.tags}
              />
              <Preview generator={this.generatePreview} />
            </div>
          )}
      </div>
    )
  }
}

export default DragDropContext(MultiBackend(HTML5toTouch))(Composite)

const getPrice = (a, b = 1) => {
  return parseFloat(a) * parseFloat(b)
}

const sendPost = (url, post) => {
  const data = JSON.stringify(post)
  const options = {
    method: 'post',
    // mode: "no-cors",
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: data
  }
  return fetch(url, options)
}

const normalizeItems = arr => {
  return arr.map((item, idx) => {
    item.image = normalizeImage(item.image_first)
    item.imageDrag = normalizeImage(item.images[1])
    item.banner = item.images[1]
    item.idx = idx
    return item
  })
}

const normalizeImage = image => {
  if (image) {
    const pathArray = image.split('/')
    const fileArray = pathArray[pathArray.length - 1].split('.')
    fileArray[fileArray.length - 1] = 'png'
    pathArray[pathArray.length - 1] = fileArray.join('.')
    pathArray.splice(pathArray.length - 1, 0, '100x100')
    return pathArray.join('/')
  }
}

const createCollectionArray = size => {
  const collectionArray = []
  for (let index = 0; index < size; index++) {
    collectionArray[index] = { accepts: [ItemTypes.FOOD], lastDroppedItem: null }
  }
  return collectionArray
}

const updateHeader = () => {
  const header = document.querySelector('#header-order')
  if (header) {
    axios.get('/api/getMiniCart').then(response => {
      header.innerHTML = response.data
      axios
        .get('/api/get-cart')
        .then(cart => cart.json())
        .then(cart => {
          console.log(cart)
        })
    })
  }
}
