import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DropTarget } from 'react-dnd'
import EasyTransition from 'react-easy-transition'
import cx from 'classnames'

const dustbinTarget = {
  drop(props, monitor) {
    props.onDrop(monitor.getItem())
  }
}

@DropTarget(props => props.accepts, dustbinTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class Dustbin extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    accepts: PropTypes.arrayOf(PropTypes.string).isRequired,
    lastDroppedItem: PropTypes.object,
    onDrop: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props)
    this.state = {
      droppedBoxNames: []
    }
  }
  render() {
    const { isOver, canDrop, connectDropTarget, lastDroppedItem } = this.props
    const isActive = isOver && canDrop

    let backgroundColor = '#FFF9EA'
    if (isActive) {
      backgroundColor = '#bdd2a1'
    } else if (canDrop) {
      backgroundColor = '#E9F0E0'
    }
    const dropClass = cx({
      dropBox: true,
      dropBoxBig: this.props.columns <= 4
    })
    return connectDropTarget(
      <div className={dropClass} style={{ backgroundColor }}>
        {lastDroppedItem && (
          <div>
            <EasyTransition
              path={window.location.pathname}
              initialStyle={{ opacity: 0, transform: 'scale(0)' }}
              transition="all 0.3s ease-in"
              finalStyle={{ opacity: 1, transform: 'scale(1)' }}
              leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
              <img src={'http://lavka.monkeylab.ru/' + lastDroppedItem.imageDrag} alt="ss" />
            </EasyTransition>
            <div className="dropBoxRemove" onClick={this.props.remove}>
              <div className="dropBoxRemoveIcon">
                <svg height="32" width="32" viewBox="0 0 32 32">
                  <g fill="none">
                    <path d="M16 0C7.2 0 0 7.2 0 16s7.2 16 16 16 16-7.2 16-16S24.8 0 16 0z" fill="#FFF9EA" />
                    <path d="M20.72 11L11 20.71M20.72 20.71L11 11" stroke="#979797" strokeLinecap="round" />
                  </g>
                </svg>
              </div>
            </div>
          </div>
        )}
        <style jsx>{`
          .dropBox {
            box: vertical middle center;
            flex: 1 1 80px;
            max-width: 80px;
            min-width: 80px;
            margin-bottom: -2px;
            margin-left: -2px;
            border: 2px solid #efe5ce;
            border-radius: 5px;
            size: 80px;
            position: relative;
            &:hover {
              .dropBoxRemove {
                opacity: 1;
                visibility: visible;
              }
            }
          }
          .dropBoxBig {
            flex: 1 1 150px;
            max-width: 150px;
            min-width: 150px;
            margin-bottom: -2px;
            margin-left: -2px;
            border: 2px solid #efe5ce;
            border-radius: 5px;
            size: 150px;
            .dropBoxRemove {
              &:after {
                size: 142px;
              }
            }
          }

          .dropBox img {
            width: 100%;
            object-fit: cover;
          }
          .dropBoxRemove {
            position: absolute;
            size: 100%;
            top: 0;
            left: 0;
            opacity: 0;
            visibility: hidden;
            transition: all 0.4s;
            &:after {
              display: block;
              position: absolute;
              content: '';
              size: 72px;
              top: 2px;
              left: 2px;
              opacity: 0.43;
              background: #4a4a4a;
              border-radius: 5px;
              z-index: 10;
            }
          }

          .dropBoxRemoveIcon {
            opacity: 1;
            position: absolute;
            z-index: 11;
            left: center;
            top: center;
            svg {
              size: ;
            }
          }
        `}</style>
      </div>
    )
  }
}
