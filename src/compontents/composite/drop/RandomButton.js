import React from 'react'

const RandomButton = props => {
  return (
    <button className="butt" onClick={props.onClick}>
      <div className="icon">
        <svg height="14" width="15" viewBox="0 0 15 14">
          <g fill="#4A4A4A">
            <path d="M12.023.65l1.498 1.61h-.957c-1.292 0-2.542.53-3.417 1.434l-5 5.245c-.708.745-1.73 1.177-2.792 1.177H.417c-.23 0-.417.176-.417.393 0 .217.187.393.417.393h.937c1.292 0 2.542-.53 3.417-1.434l5-5.245c.71-.747 1.73-1.18 2.793-1.18h1.166l-1.703 1.808c-.152.162-.135.41.04.552.17.14.43.124.578-.037l2.218-2.378c.138-.148.137-.37-.002-.515L12.647.13c-.15-.158-.408-.174-.578-.035L12.063.1c-.172.14-.19.387-.04.55" />
            <path d="M4.395 5.03l.83.837c.164.165.442.178.623.03l.006-.006c.182-.15.196-.408.03-.574L5.06 4.48C4.13 3.55 2.805 3 1.436 3H.442C.198 3 0 3.182 0 3.406c0 .224.198.406.442.406h.994c1.126 0 2.208.446 2.96 1.217M11.97 8.886l1.736 1.908H12.54c-1.082 0-2.122-.455-2.843-1.24l-.88-.953c-.158-.17-.43-.184-.605-.03s-.19.42-.03.59l.88.952c.89.95 2.14 1.51 3.477 1.51h.954l-1.524 1.696c-.154.17-.136.432.04.582.176.148.44.13.594-.04l2.257-2.504c.14-.155.14-.388 0-.543l-2.25-2.48c-.156-.172-.425-.186-.6-.034l-.008.007c-.172.152-.187.41-.033.58" />
          </g>
        </svg>
      </div>
      <span>Собрать случайные вкусы</span>
      <style jsx>{`
        .butt {
          padding: 0;
          background-color: transparent;
          border: 0;
          color: #cfbf99;
          font-size: 14px;
          line-height: 12px;
          letter-spacing: 0;
          outline: 0;
          cursor: pointer;
          box: horizontal middle left;
          transition: all 0.4s;
          &:hover {
            span {
              border-color: #cfbf99;
              color: #cfbf99;
            }
            .icon {
              background-color: #cfbf99;
            }
          }
        }
        span {
          border-bottom: 1px solid #cbdfc3;
          color: #cbdfc3;
          letter-spacing: 0.3px;
          transition: all 0.4s;
        }
        .icon {
          size: 30px;
          background: #e9f0e0;
          border-radius: 50%;
          margin-right: 10px;
          box: vertical middle center;
          transition: all 0.4s;
        }
      `}</style>
    </button>
  )
}

export default RandomButton
