import React, { Component } from 'react'

import css from 'styled-jsx/css'

import DropItem from './DropItem'
import Dustbin from './Dustbin'

import Expand from './Expand'
import ItemTypes from './ItemTypes'
import RandomButton from './RandomButton'

import { SpringGrid, layout } from 'react-stonecutter'

export default class Drop extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // dustbins: createCollectionArray(this.props.size),
      droppedBoxNames: [],
      size: this.props.size,
      boxFull: false
    }
  }



  isDropped(boxName) {
    return this.state.droppedBoxNames.indexOf(boxName) > -1
  }

  handleSort() {
    const arr = this.props.collection.sort((a, b) => {
      if (a.lastDroppedItem === null) {
        return 1
      } else if (b.lastDroppedItem === null) {
        return -1
      } else if (a.lastDroppedItem.id === b.lastDroppedItem.id) {
        return 0
      }
      return a.lastDroppedItem.id < b.lastDroppedItem.id ? -1 : 1
    })
    this.setState({ dustbins: arr })
  }

  handleRandom() {
    const arr = this.props.collection.map(item => {
      item.lastDroppedItem = this.props.items[Math.floor(Math.random() * this.props.items.length)]
      return item
    })

    arr.sort((a, b) => {
      if (a.lastDroppedItem === null) {
        return 1
      } else if (b.lastDroppedItem === null) {
        return -1
      } else if (a.lastDroppedItem.id === b.lastDroppedItem.id) {
        return 0
      }
      return a.lastDroppedItem.id < b.lastDroppedItem.id ? -1 : 1
    })

    this.props.updateCollection(arr)
  }

  handleClear() {
    const arr = this.props.collection.map(item => {
      item.lastDroppedItem = null
      return item
    })
    this.props.updateCollection(arr)
  }

  handleClick(item) {
    const inx = this.props.collection.findIndex(elem => !elem.lastDroppedItem)
    if (inx !== -1) {
      const arr = this.props.collection
      arr[inx].lastDroppedItem = item
      this.props.updateCollection(arr)
    } else {
      this.setState({ boxFull: true })
    }
  }

  handleRemove(index) {
    let arr = this.props.collection
    this.props.collection[index].lastDroppedItem = null
    this.props.updateCollection(arr)
  }

  handleDrop(index, item) {
    let arr = this.props.collection.map((itemArr, indexArr) => {
      if (index === indexArr) itemArr.lastDroppedItem = item
      return itemArr
    })

    this.props.updateCollection(arr)
  }

  handleExpand() {
    const collectionArray = this.props.collection
    const size = parseFloat(this.state.size) + 6
    for (let index = collectionArray.length; index < size; index++) {
      collectionArray[index] = { accepts: [ItemTypes.FOOD], lastDroppedItem: null }
    }
    this.setState({ boxFull: false, size: size })
    this.props.updateCollection(collectionArray)
  }
  render() {
    const items = this.props.items

    return (
      <div>
        <div className="dropZone">
          <div className="dropLeft">
            <div className="dropControl">
              {/* <button className="dropClear" onClick={() => this.handleSort()}>
                Упорядочить
              </button> */}
              <button className="dropClear" onClick={() => this.handleClear()}>
                Очистить все
              </button>
            </div>

            <div className="dropBoxes" style={{ overflow: 'hidden', clear: 'both' }}>
              <SpringGrid
                component="div"
                columns={parseFloat(this.props.columns)}
                columnWidth={this.props.columns > 4 ? 78 : 148}
                gutterWidth={0}
                gutterHeight={0}
                itemHeight={this.props.columns > 4 ? 78 : 148}
                layout={layout.simple}
                springConfig={{ stiffness: 170, damping: 26 }}>
                {this.props.collection.map(({ accepts, lastDroppedItem }, index) => (
                  <div key={'A' + index}>
                    <Dustbin
                      accepts={accepts}
                      lastDroppedItem={lastDroppedItem}
                      onDrop={item => this.handleDrop(index, item)}
                      index={index}
                      key={'A' + index}
                      remove={() => this.handleRemove(index)}
                      columns={this.props.columns}
                    />
                  </div>
                ))}
              </SpringGrid>
            </div>

            <div className="dropControlBottom">
              <div className="dropCount">
                Уже в коробке: {countDrops(this.props.collection)} из {this.state.size}
              </div>
            </div>
            {this.props.collection.filter(item => item.lastDroppedItem != null).length ===
            parseFloat(this.state.size) ? (
              <Expand expand={() => this.handleExpand()} />
            ) : null}
          </div>
          <div className="dropRight">
            <div className="dropItemsHeader">
              <RandomButton onClick={() => this.handleRandom()} />
            </div>
            <div className="dropItems" style={{ overflow: 'hidden', clear: 'both' }}>
              {items.map((item, index) => (
                <DropItem
                  id={item.id}
                  name={item.name}
                  onClick={() => this.handleClick(item)}
                  image={item.image}
                  imageDrag={item.imageDrag}
                  type={ItemTypes.FOOD}
                  isDropped={this.isDropped(item)}
                  key={index}
                />
              ))}
            </div>
          </div>
        </div>

        <style jsx>{styles}</style>
      </div>
    )
  }
}

const countDrops = arr => {
  const empties = arr.reduce((x, y) => {
    if (y.lastDroppedItem === null) {
      return x - 1
    }
    return x
  }, arr.length)
  return empties
}

const styles = css`.dropZone
    box horizontal top
    padding 0 20px
    flex-wrap wrap

.dropZoneMobile
    position fixed
    bottom 0
    size 100% 50px
    z-index 1000


.dropLeft
    lost-column 1 / 2 2 50px flex
    box vertical top right
    @media(max-width: 1024px)
        display none

.dropRight
    lost-column 1 / 2 2 50px flex
    @media(max-width: 1024px)
        lost-column 1 / 1 1 50px flex


.dropBoxes
    box horizontal top right
    // max-width 480px
    margin-bottom 11px
    padding-bottom 2px
    padding-left 2px
    flex-wrap wrap



.dropItems
    box horizontal top left
    flex-wrap wrap
    max-width 460px
    @media(max-width: 1024px)
        max-width 100%
        margin-top 20px

.dropItemsHeader
    color #9B9B9B
    font-size 13px
    line-height 22px
    letter-spacing .65px
    margin-bottom 10px
    padding-left 12px

.dropControl
    box horizontal middle right
    width 480px
    padding-left 10px
    margin-bottom 13px
    padding-top 9px
.dropControlBottom
    box horizontal middle right
    width 480px
    padding-left 10px
    margin-bottom 26px
.dropCount
    color #4A4A4A
    font-size 14px
    letter-spacing 0

.dropClear
    padding 0
    background-color transparent
    border 0
    border-bottom 1px solid #CFBF99
    color #CFBF99
    font-size 14px
    line-height 12px
    letter-spacing 0
    outline 0
    cursor pointer`
