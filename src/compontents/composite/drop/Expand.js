import EasyTransition from 'react-easy-transition'
import React from 'react'

const Expand = () => {
  return (
    <EasyTransition
      path={window.location.pathname}
      initialStyle={{ opacity: 0, transform: 'scale(0)' }}
      transition="all 0.3s ease-in"
      finalStyle={{ opacity: 1, transform: 'scale(1)' }}
      leaveStyle={{ opacity: 0, transform: 'scale(0)' }}>
      <div className="expand" key="exp">
        <div className="icon">
          <svg height="31" width="31" viewBox="0 0 31 31">
            <g fill="none">
              <path
                d="M15.5 0C6.975 0 0 6.975 0 15.5S6.975 31 15.5 31 31 24.025 31 15.5 24.025 0 15.5 0z"
                fill="#E9F0E1"
              />
              <path d="M10 14.605l4.17 4.17L20.947 12" stroke="#979797" strokeLinecap="round" />
            </g>
          </svg>
        </div>
        <div className="content">
          Ваш набор собран. Вы можете оформить
          {/* <Scrollchor className="link" to="#compositeBottom" animate={{ offset: 20, duration: 600 }}>
            заказ
          </Scrollchor>. */}
          <a href="#compositeBottom" className="link">
            заказ
          </a>.
          {/* или выбрать коробку  <span className="link" onClick={props.expand}>
            больше
          </span> */}
        </div>

        <style global jsx>{`
          .link {
            display: inline-block;
            position: relative;
            cursor: pointer;
            text-decoration: none;
            color: #4a4a4a;
            &:hover {
              text-decoration: none;
              color: #4a4a4a;
              &:after {
                opacity: 0;
              }
            }
            &:after {
              display: block;
              position: absolute;
              bottom: 4px;
              size: 100% 1px;
              background-color: #4a4a4a;
              content: '';
              transition: all 0.4s;
            }
          }
        `}</style>
        <style jsx>{`
          .expand {
            box: horizontal top left;
            max-width: 460px;
          }
          .icon {
            margin: 0 10px 0 0;
          }
          .content {
            font-size: 20px;
            line-height: 26px;
            font-weight: 300;
            color: #4a4a4a;
            letter-spacing: 0.8px;
          }
        `}</style>
      </div>
    </EasyTransition>
  )
}

export default Expand
