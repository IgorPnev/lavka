import PropTypes from "prop-types"
import React, { Component } from "react"
import { DropTarget } from "react-dnd"
import EasyTransition from "react-easy-transition"

// import './Dustbin.styl'
// import ItemTypes from './ItemTypes'

const dustbinTarget = {
  drop(props, monitor) {
    props.onDrop(monitor.getItem())
  }
}

@DropTarget(props => props.accepts, dustbinTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class DustbinMobile extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    accepts: PropTypes.arrayOf(PropTypes.string).isRequired,
    lastDroppedItem: PropTypes.object,
    onDrop: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props)
    this.state = {
      droppedBoxNames: []
    }
  }
  render() {
    const { isOver, canDrop, connectDropTarget, lastDroppedItem } = this.props
    const isActive = isOver && canDrop

    let backgroundColor = "#FFF9EA"
    if (isActive) {
      backgroundColor = "#bdd2a1"
    } else if (canDrop) {
      backgroundColor = "#E9F0E0"
    }

    return connectDropTarget(
      <div className="dropBox dropBox16 swiper-slide" style={{ backgroundColor }}>
        {lastDroppedItem && (
          <div>
            <EasyTransition
              path={window.location.pathname}
              initialStyle={{ opacity: 0, transform: "scale(0)" }}
              transition="all 0.3s ease-in"
              finalStyle={{ opacity: 1, transform: "scale(1)" }}
              leaveStyle={{ opacity: 0, transform: "scale(0)" }}>
              <img src={"http://lavka.monkeylab.ru/" + lastDroppedItem.image} alt="ss" />
            </EasyTransition>
            <div className="dropBoxRemove" onClick={this.props.remove}>
              <div className="dropBoxRemoveIcon">
                <svg height="32" width="32" viewBox="0 0 32 32">
                  <g fill="none">
                    <path d="M16 0C7.2 0 0 7.2 0 16s7.2 16 16 16 16-7.2 16-16S24.8 0 16 0z" fill="#FFF9EA" />
                    <path d="M20.72 11L11 20.71M20.72 20.71L11 11" stroke="#979797" strokeLinecap="round" />
                  </g>
                </svg>
              </div>
            </div>
          </div>
        )}
        <style jsx>{`.dropBox
    box: vertical middle center;
    flex: 1 1 50px;
    max-width: 50px;
    min-width: 50px;
    margin-bottom: -2px;
    margin-left: -2px;
    border: 1px solid #efe5ce;
    border-radius: 5px;
    size: 50px;
    position: relative;
    &:hover2
        .dropBoxRemove
            opacity 1
            visibility visible

.dropBox img
    width 100%
    object-fit cover

.dropBoxRemove
    position absolute
    top 0
    left 0
    opacity 0
    visibility hidden
    transition all .4s
    size 100%
    &:after
        display block
        position absolute
        top 2px
        left 2px
        z-index 10
        opacity .43
        background #4a4a4a
        content ''
        border-radius 5px
        size 42px

.dropBoxRemoveIcon
    position absolute
    top center
    left center
    z-index 11
    opacity 1
    svg
        size`}</style>
      </div>
    )
  }
  handleClick() {
    console.log(this.props)
  }
}

// {isActive ? 'Release to drop' : `This dustbin accepts: ${accepts.join(', ')}`}
// <img src={'/build/' + lastDroppedItem.image} alt="ss" />}
