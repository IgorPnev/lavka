import React, { Component } from 'react'
import { DragSource } from 'react-dnd'
import css from 'styled-jsx/css'

const boxSource = {
  beginDrag(props) {
    return {
      id: props.id,
      name: props.name,
      image: props.image,
      imageDrag: props.imageDrag
    }
  }
}

@DragSource(props => props.type, boxSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  connectDragPreview: connect.dragPreview(),
  isDragging: monitor.isDragging()
}))
class DropItem extends Component {
  componentDidMount() {
    const img = new Image()
    img.onload = () => this.props.connectDragPreview(img)
    img.src = 'http://lavka.monkeylab.ru/' + this.props.imageDrag
  }

  render() {
    const { name, isDragging, connectDragSource } = this.props
    const opacity = isDragging ? 0.4 : 1
    return connectDragSource(
      <div className="dropItem" style={{ opacity }} onClick={this.props.onClick}>
        <img src={'http://lavka.monkeylab.ru/' + this.props.image} alt="ss" />
        <p>{name}</p>
        <style jsx>{styles}</style>
      </div>
    )
  }
}

export default DropItem

const styles = css`
  .dropItem {
    box: vertical top center;
    width: 95px;
    margin-right: 20px;
    margin-bottom: 5px;
    overflow: hidden;
    &:active {
      img {
        transform scale(0.9);
      }
    }
    img {
      margin-bottom: 0;
      size: 75px;
      object-fit: contain;
      transition: all .2s easeInOutCubic;
    }
    p {
      color: #4a4a4a;
      font-size: 13px;
      line-height: 15px;
      letter-spacing: 0.41px;
      text-align: center;
      font-width: 300;
    }
  }
`
