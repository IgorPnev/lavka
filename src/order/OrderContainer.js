import './OrderContainer.styl'

import React, { Component } from 'react'

import CartContainer from '../cart/CartContainer.js'
import Layout from './layout/Layout'
// import { connect } from 'react-redux'
import { hot } from 'react-hot-loader'

class OrderContainer extends Component {
  render() {
    return (
      <div className="order">
        <div>
          <Layout />
        </div>
        <CartContainer />
      </div>
    )
  }
}

export default hot(module)(OrderContainer)
