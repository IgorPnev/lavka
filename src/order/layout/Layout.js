import './Layout.styl'

import React, { Component, Fragment } from 'react'
import { Step, Steps, Wizard } from 'react-albus'

import BottomNavigation from './BottomNavigation'
import TopNavigation from './TopNavigation'
import User from '../user/user'

class Layout extends Component {
  state = {}

  render() {
    return (
      <Fragment>
        <Wizard>
          <div className="orderTop">
            <div className="orderStepHeader">ОФОРМЛЕНИЕ ЗАКАЗА</div>
            <TopNavigation />
          </div>
          <div className="orderForm">
            <Steps>
              <Step id="user" className="step">
                <User />
              </Step>
              <Step id="delivere" className="step">
                <h1 className="text-align-center">Delivere</h1>
              </Step>
              <Step id="payment" className="step">
                <h1 className="text-align-center">Payment</h1>
              </Step>
            </Steps>
            <BottomNavigation />
          </div>
        </Wizard>
      </Fragment>
    )
  }
}

export default Layout
