import './TopNavigation.styl'

import React from 'react'
import { WithWizard } from 'react-albus'

const TopNavigation = () => (
  <WithWizard
    render={({ step, steps, go }) => (
      <div className="orderSteps">
        <div className={steps.indexOf(step) === 0 ? 'orderStep orderStepActive' : 'orderStep'} onClick={() => go(0)}>
          <span>1</span>
        </div>

        <svg height="10" width="4" viewBox="0 0 4 10">
          <path d="M0 1l3.899 3.788L.016 8.561" fill="none" stroke="#DFD3B4" strokeLinecap="round" />
        </svg>

        <div className={steps.indexOf(step) === 1 ? 'orderStep orderStepActive' : 'orderStep'} onClick={() => go(1)}>
          <span>2</span>
        </div>

        <svg height="10" width="4" viewBox="0 0 4 10">
          <path d="M0 1l3.899 3.788L.016 8.561" fill="none" stroke="#DFD3B4" strokeLinecap="round" />
        </svg>

        <div className={steps.indexOf(step) === 2 ? 'orderStep orderStepActive' : 'orderStep'} onClick={() => go(2)}>
          <span>3</span>
        </div>
      </div>
    )}
  />
)

export default TopNavigation
