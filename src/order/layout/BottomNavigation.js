import './BottomNavigation.styl'

import React from 'react'
import { WithWizard } from 'react-albus'

const BottomNavigation = () => (
  <WithWizard
    render={({ next, previous, step, steps }) => (
      <div className="bottomNav">
        <button type="button" className="bottomNavButtonBack" disabled={!(steps.indexOf(step) > 0)} onClick={previous}>
          Вернутся
        </button>

        <button
          type="button"
          className="bottomNavButtonNext"
          disabled={!(steps.indexOf(step) < steps.length - 1)}
          onClick={next}>
          Далее
        </button>
      </div>
    )}
  />
)

export default BottomNavigation
