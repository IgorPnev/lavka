import './User.styl'

import { Field, Form, FormSpy } from 'react-final-form'
import React, { Fragment } from 'react'

import Mask from '../../compontents/form/mask/Mask'
import { connect } from 'react-redux'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  console.log(JSON.stringify(values, 0, 2))
}

const User = ({ requestDiscount, profile, updateProfile }) => {
  return (
    <div className="user">
      <Fragment>
        <Form
          onSubmit={onSubmit}
          initialValues={profile.profile.profile}
          render={({ handleSubmit, submitting, pristine, values }) => (
            <Fragment>
              <FormSpy
                subscription={{ values: true }}
                onChange={value => {
                  onSubmit(value)
                }}
              />
              <form onSubmit={handleSubmit}>
                <div className="formItem">
                  <label className="label">Имя и фамилия</label>
                  <Field name="name" component="input" type="text" className="input" placeholder="Имя и фамилия" />
                </div>
                <div className="formItem">
                  <label className="label">Email</label>
                  <Field name="email" className="input" component="input" type="email" placeholder="example@email.ru" />
                  <label className="checkbox">
                    <Field name="email_subscribe" component="input" type="checkbox" />
                    <div className="checkbox__text">Получать новости от «Счастья»</div>
                  </label>
                </div>
                <div className="formItem">
                  <label className="label">Телефон</label>
                  <Field
                    name="phone"
                    className="input"
                    component="input"
                    type="text"
                    placeholder="+7 (812) 123-23-23"
                  />
                  <label className="checkbox">
                    <Field name="sms_subscribe" component="input" type="checkbox" />{' '}
                    <div className="checkbox__text">Получать новости по SMS</div>
                  </label>
                </div>
                <div className="formItem">
                  <label className="checkbox">
                    <Field name="rules" component="input" type="checkbox" />
                    <div className="checkbox__text">
                      Я даю согласие на обработку <a href="/static/rules.pdf">персональных данных</a>
                    </div>
                  </label>
                </div>

                {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
              </form>
            </Fragment>
          )}
        />
      </Fragment>
    </div>
  )
}

function mapStateToProps(store) {
  return {
    profile: store.profile
  }
}

export default connect(mapStateToProps, undefined)(User)
