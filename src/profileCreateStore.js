import { createStore as _createStore, applyMiddleware, compose } from 'redux'

import createSagaMiddleware from 'redux-saga'
import reducer from './reducer'
import thunk from 'redux-thunk'

export const sagaMiddleware = createSagaMiddleware()

export default function createStore() {
  const middleware = [sagaMiddleware, thunk]

  const finalCreateStore = compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )(_createStore)

  const store = finalCreateStore(reducer)

  if (module.hot) {
    module.hot.accept('./reducer', () => {
      store.replaceReducer(require('./reducer'))
    })
  }

  return store
}
