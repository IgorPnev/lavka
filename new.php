<!DOCTYPE html>
<html lang="ru" class="no-js">
<head>
  [[$head]]
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css" class="next-head" />
</head>
<body class="page">
  [[!$icons]]
  [[$body__header]]
  <div id="order" class="good-part">
  <div class="part-card">
      [[msGallery?
        &product=`[[*id]]`
        &tpl=`@INLINE {$files[0]['1366x420']}`
        &toPlaceholder=`background-image`
        &limit=`1`
        &offset=`1`
      ]]
      <div style="background-image: url([[+background-image]])" class="part-card__image"></div>
    </div>
 </div>



  <div>
      <script>
              var start = {
                id: [[*id]],
                category: [[*id]],
                items: [[*id]],
                size: [[*technical]]
              };
            </script>
        <div id="root"></div>
        <script type="text/javascript" src="/assets/js/main.c98b20b5.js"></script>
            </div>
            <div id="peppermint" class="peppermint peppermint-inactive box-variant-gallery box-variant-gallery_hidden_mobile">
              [[+box-variant-gallery]]
            </div>
            <div class="part-goods part-goods_similar">
              <h2 class="part-goods__title">С этим товаром покупают</h2>
              [[msProducts:toPlaceholder=`includeParent`?
                &returnIds=`1`
                &parents=`[[*parent]]`
                &resources=`-[[*id]]`
                &fastMode=`1`
                &limit=`5`
              ]]
              [[msProducts:toPlaceholder=`includeLinks`?
                &returnIds=`1`
                &parents=`0`
                &fastMode=`1`
                &limit=`5`
                &link=`3`
                &master=`[[*id]]`
              ]]
              [[!msProducts?
                &showLog=`0`
                &fastMode=`1`
                &parents=`2,3,4,5,6,7`
                &resources=`[[+includeParent]],[[+includeLinks]]`
                &leftJoin=`{
                  "origStnd": { "class":"msProductFile", "alias":"origStnd", "on":"origStnd.product_id = msProduct.id AND origStnd.parent = 0 AND origStnd.rank = 0"}
                , "stnd": { "class":"msProductFile", "alias":"stnd", "on":"stnd.product_id = msProduct.id AND stnd.path LIKE '%/260x260/' AND stnd.name = origStnd.name"}
                }`
                &select=`{
                  "msProduct":"id, class_key, pagetitle, longtitle, price, uri"
                , "stnd":"stnd.url as stnd"
                }`
                &limit=`5`
                &sortby=`RAND()`
                &tpl=`good-relevant__item`
              ]]
            </div>
            [[$body__footer]]
            [[$body__script]]
</body>

</html>