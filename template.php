<!DOCTYPE html>
<html>

<head>
  <title class="next-head">Home page</title>
  <meta charset="[[++modx_charset]]">
  <meta charset="utf-8" class="next-head" />
  <meta name="viewport" content="width=device-width, user-scalable=no" class="next-head" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css" class="next-head" />
  <link rel="preload" href="/assets/_next/c045517f-d871-474f-ae02-577d4934dd18/page/index.js" as="script" />
  <link rel="preload" href="/assets/_next/c045517f-d871-474f-ae02-577d4934dd18/page/_error.js" as="script" />
  <link rel="preload" href="/assets/_next/8326ba862c63c3a9eb364bf6325d2c71/app.js" as="script" />
  <style id="__jsx-326463437">
    .composite.jsx-326463437 {
      display: block;
      height: 100%;
      min-height: 100vh;
      padding-top: 30px;
      background-color: #fff9ea;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
      -webkit-box-pack: start;
      -webkit-justify-content: flex-start;
      -ms-flex-pack: start;
      justify-content: flex-start;
    }

    .compositeHeader.jsx-326463437 {
      margin-bottom: 30px;
      color: #4a4a4a;
      padding: 0 20px;
      font-size: 20px;
      font-weight: 300;
      -webkit-letter-spacing: 0.8px;
      -moz-letter-spacing: 0.8px;
      -ms-letter-spacing: 0.8px;
      letter-spacing: 0.8px;
      text-align: center;
      text-transform: uppercase;
    }
  </style>
  <style id="__jsx-1756794767">
    html {
      font-family: sans-serif;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
    }

    body {
      margin: 0;
    }

    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    header,
    main,
    menu,
    nav,
    section,
    summary {
      display: block;
    }

    audio,
    canvas,
    progress,
    video {
      display: inline-block;
    }

    audio:not([controls]) {
      display: none;
      height: 0;
    }

    progress {
      vertical-align: baseline;
    }

    template,
    [hidden] {
      display: none;
    }

    a {
      background-color: transparent;
      -webkit-text-decoration-skip: objects;
    }

    a:active,
    a:hover {
      outline-width: 0;
    }

    abbr[title] {
      border-bottom: none;
      text-decoration: underline;
      text-decoration: underline dotted;
    }

    b,
    strong {
      font-weight: inherit;
    }

    b,
    strong {
      font-weight: bolder;
    }

    dfn {
      font-style: italic;
    }

    h1 {
      font-size: 2em;
      margin: 0.67em 0;
    }

    mark {
      background-color: #ff0;
      color: #000;
    }

    small {
      font-size: 80%;
    }

    sub,
    sup {
      font-size: 75%;
      line-height: 0;
      position: relative;
      vertical-align: baseline;
    }

    sub {
      bottom: -0.25em;
    }

    sup {
      top: -0.5em;
    }

    img {
      border-style: none;
    }

    svg:not(:root) {
      overflow: hidden;
    }

    code,
    kbd,
    pre,
    samp {
      font-family: monospace, monospace;
      font-size: 1em;
    }

    figure {
      margin: 1em 40px;
    }

    hr {
      box-sizing: content-box;
      height: 0;
      overflow: visible;
    }

    button,
    input,
    optgroup,
    select,
    textarea {
      color: inherit;
      font: inherit;
      margin: 0;
      vertical-align: middle;
    }

    optgroup {
      font-weight: bold;
    }

    button,
    input {
      overflow: visible;
    }

    button,
    select {
      text-transform: none;
    }

    button,
    html [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
    }

    button:-moz-focusring,
    [type="button"]:-moz-focusring,
    [type="reset"]:-moz-focusring,
    [type="submit"]:-moz-focusring {
      outline: 1px dotted ButtonText;
    }

    fieldset {
      border: 1px solid #c0c0c0;
      margin: 0 2px;
      padding: 0.35em 0.625em 0.75em;
    }

    legend {
      box-sizing: border-box;
      color: inherit;
      display: table;
      max-width: 100%;
      padding: 0;
      white-space: normal;
    }

    textarea {
      overflow: auto;
      resize: none;
      vertical-align: top;
    }

    input,
    select,
    textarea {
      outline: 0;
    }

    [disabled] {
      cursor: default;
    }

    [type="checkbox"],
    [type="radio"] {
      box-sizing: border-box;
      padding: 0;
    }

    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
      height: auto;
    }

    [type="search"] {
      -webkit-appearance: textfield;
      outline-offset: -2px;
    }

    [type="search"]::-webkit-search-cancel-button,
    [type="search"]::-webkit-search-decoration {
      -webkit-appearance: none;
    }

    input::-moz-placeholder,
    textarea::-moz-placeholder {
      color: inherit;
      opacity: 0.54;
    }

    input:-ms-input-placeholder,
    textarea:-ms-input-placeholder {
      color: inherit;
      opacity: 0.54;
    }

    input::-webkit-input-placeholder,
    textarea::-webkit-input-placeholder {
      color: inherit;
      opacity: 0.54;
    }

    input::-ms-clear,
    input::-ms-reveal {
      display: none;
    }

     ::-webkit-file-upload-button {
      -webkit-appearance: button;
      font: inherit;
    }

    table {
      border-collapse: collapse;
      border-spacing: 0;
    }

    td,
    th {
      padding: 0;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p,
    figure,
    form,
    blockquote {
      margin: 0;
    }

    ul,
    ol,
    li,
    dl,
    dd {
      margin: 0;
      padding: 0;
    }

    ul,
    ol {
      list-style: none outside none;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-size: 100%;
      font-weight: normal;
    }

    * {
      box-sizing: border-box;
      -webkit-font-smoothing: antialiased;
      -webkit-overflow-scrolling: touch;
    }

    html {
      height: 100%;
      font-size: 16px;
    }

    body {
      font-family: 'proxima-soft', Arial, Helvetica, sans-serif;
      -webkit-font-smoothing: antialiased;
    }
  </style>
</head>

<body>
  <div>
      <script>
              var start = {
                id: [[*id]],
                category: 349,
                items: 349,
                size: [[*technical]]
              };
            </script>

    <div id="__next">
      <div data-reactroot="">
        <main class="jsx-1756794767">
          <div class="jsx-1756794767">
            <script>
              var start = {
                id: [[*id]],
                category: 349,
                items: 349,
                size: [[*technical]]
              };
            </script>
          </div>
          <div class="jsx-326463437 composite"></div>
        </main>
      </div>
    </div>
    <div id="__next-error"></div>
  </div>
  <div>
    <script>
      __NEXT_DATA__ = {
        "props": {},
        "pathname": "/",
        "query": {},
        "buildId": "c045517f-d871-474f-ae02-577d4934dd18",
        "buildStats": {
          "app.js": {
            "hash": "8326ba862c63c3a9eb364bf6325d2c71"
          }
        },
        "assetPrefix": "",
        "nextExport": true,
        "err": null,
        "chunks": []
      }
      module = {}
      __NEXT_LOADED_PAGES__ = []
      __NEXT_LOADED_CHUNKS__ = []

      __NEXT_REGISTER_PAGE = function (route, fn) {
        __NEXT_LOADED_PAGES__.push({
          route: route,
          fn: fn
        })
      }

      __NEXT_REGISTER_CHUNK = function (chunkName, fn) {
        __NEXT_LOADED_CHUNKS__.push({
          chunkName: chunkName,
          fn: fn
        })
      }
    </script>
    <script async="" id="__NEXT_PAGE__/" type="text/javascript" src="/assets/_next/c045517f-d871-474f-ae02-577d4934dd18/page/index.js"></script>
    <script async="" id="__NEXT_PAGE__/_error" type="text/javascript" src="/assets/_next/c045517f-d871-474f-ae02-577d4934dd18/page/_error.js"></script>
    <div></div>
    <script type="text/javascript" src="/assets/_next/8326ba862c63c3a9eb364bf6325d2c71/app.js" async=""></script>
  </div>
</body>

</html>